#!/usr/bin/env python
from pdbfixer import PDBFixer
from simtk.openmm.app import PDBFile
from simtk.openmm.vec3 import Vec3
import urllib.request
import os

class phmdPDBPreparer():
    def __init__(self, pdbID=None, pdbFile=None, chainID=['A'], mutate=False,
                 removeHydrogens=True, autoMode=True, residueTypes=['Asp', 'Glu', 'His', 'Cys', 'Lys']):
        """Initiate an instance of phmdPDBPreparer class

        A class/function to remove unwanted chains, models, waters, ions, ligands;
        add missing residues, atoms; build water box or memberanes.
        :param pdbID: 4-letter PDB ID; if used alone, the corresponding PDB file will be downloaded from RCSB
        :type pdbID: str
        :param pdbFile: Standard format PDB file
        :type pdbFile: str
        :param chainID: Chains to use in CpHMD simulations
        :type chainID: list of str
        :param mutate: Mutate pattern; For example, if 'ALA-133-GLY' means mutating ALA133 to GLY133.
        :type mutate: str
        :param removeHydrogens: If true, Hydrogen atoms are removed before fixing.
        :type removeHydrogens: bool
        :param autoMode: If true, all steps will be done underhood and a CpHMD-ready PDB file is generated.
        :type autoMode: bool

        :returns: An instance of the phmdPDBPreparer class
        :rtype: object
        """
        if not bool(chainID):
            raise ValueError("No chain ID is provided.")
        assert(type(chainID) is list)
        self.chainID = chainID
        self.modelID = False
        if not (bool(pdbID) or bool(pdbFile)):
            raise ValueError("No PDB file or PDB id is provided.")
        # If pdbFile is provided, pdbID will be only used as a part of the identifier.
        # If pdbFile is not provided, a corresponding pdb file to the provided pdbID will be downloaded from RCSB
        if bool(pdbFile):
            self.pdbFile = pdbFile
            if bool(pdbID):
                self.pdbID = pdbID
            else:
                self.pdbID = os.path.basename(pdbFile).split('.')[0].split('_')[0]
                try:
                    self.modelID = os.path.basename(pdbFile).split('.')[0].split('_')[1]
                except IndexError:
                    self.modelID = False
        else:
            self.pdbID = pdbID
            url = 'http://www.rcsb.org/pdb/files/{}.pdb'.format(pdbID)
            fileName = '{}.pdb'.format(pdbID)
            try:
                urllib.request.urlretrieve(url, fileName)
            except Exception:
                print("Cannot download PDB file '{}' from RCSB.".format(fileName))
                exit()
            self.pdbFile = fileName
        if removeHydrogens:
            self.removeAllHydrogens()
        self.residueTypes = [res.upper() for res in residueTypes]
        if bool(residueTypes) and type(residueTypes) != list:
            self.residueTypes = [res.upper() for res in residueTypes.split()]
        self.fixer = PDBFixer(filename=self.pdbFile)
        if self.modelID:
            self.identifier = self.pdbID + '_chain' + ''.join(self.chainID) + '_' + self.modelID
        else:
            self.identifier = self.pdbID + '_chain' + ''.join(self.chainID)
        # Keep only the wanted chain(s) and remove others
        allChainIds = [chain.id for chain in self.fixer.topology.chains()]
        idToRemove = []
        for i in allChainIds:
            if i not in self.chainID:
                idToRemove.append(i)
        self.fixer.removeChains(chainIds=idToRemove)
        if mutate:
            print('Mutate: {}'.format(mutate))
            contents = mutate.split()
            self.mutate(contents[0:-1], contents[-1])
        self.fixer.findMissingResidues()

        # Only add missing residues inside a chain, not the ones at the ends
        chains = list(self.fixer.topology.chains())
        keys = self.fixer.missingResidues.keys()
        self.fixer.findMissingResidues()
        for key in keys:
            chain = chains[key[0]]
            if key[1] == 0 or key[1] == len(list(chain.residues())):
                del self.fixer.missingResidues[key]

        self.fixer.findNonstandardResidues()
        if autoMode:
            self.tmpFileName = '{}_tmp.pdb'.format(self.identifier)
            self.fixedFileName = '{}_fixed.pdb'.format(self.identifier)  # Final fixed PDB file name
            self.rpNonstandardResidues()
            self.rmHeterogens()
            self.addMissing()
            self.saveStandardPDB()
            self.addCapping()

    def rpNonstandardResidues(self, residueName='ALA'):
        """Replace all non-standard residues

        Replace all non-standard residues with the provided residue type specified by 'residueName'
        :param residueName: The residue type name that will be used to replace non-standard residues
        :type residueName: str

        :returns: None
        :rtype: None
        """
        self.fixer.nonstandardResidues = [(residue, residueName)
                                          for residue, replacement in self.fixer.nonstandardResidues]
        self.fixer.replaceNonstandardResidues()

    def rmHeterogens(self, keepWater=False):
        """Remove heterogens

        Remove heterogens from the PDB file but water molecules can be kept by setting 'keepWater'.
        :param keepWater: If true, water molecules won't be removed.
        :type keepWater: bool

        :returns: None
        :rtype: None
        """
        self.fixer.removeHeterogens(keepWater)

    def mutate(self, mutations, chain_id):
        # TODO: tests
        """Mutate a specific residue

        Mutate a specific residue according to the mutation patterns provided and the chain ID to mutate.
        :param mutations: list of mutation patterns to perform; For example ['ALA-133-GLY'] to mutate ALA133
                            to GLY133.
        :type mutations: list of str
        :param chain_id: chain ID for mutations
        :type chain_id: str
        """
        assert(type(mutations) is list)
        self.fixer.applyMutations(mutations, chain_id)

    def addMissing(self, pH=7.0):
        """Add missing atoms

        Add missing atoms, including Hydrogen atoms, according to the provided pH condition.
        :param pH: pH condition to determine the protonation state of a residue
        :type pH: float

        :returns: None
        :rtype: None
        """
        assert(type(pH) is float)
        self.fixer.findMissingAtoms()
        self.fixer.addMissingAtoms()
        self.fixer.addMissingHydrogens(pH)  # Add other hydrogens first, leave out GL2/AS2 dummy hydrogens

    def addWaterBox(self):
        # TODO: tests
        """Add water box

        Add water box, only cubic box is supported. Do not use for GB calculations.
        """
        print('Warning: not tested yet.')
        maxSize = max(max((pos[i] for pos in self.fixer.positions))
                      - min((pos[i] for pos in self.fixer.positions)) for i in range(3))
        boxSize = maxSize*Vec3(1, 1, 1)
        self.fixer.addSolvent(boxSize)
        self.fixer.addSolvent(self.fixer.topology.getUnitCellDimensions())

    def saveStandardPDB(self, fileName=None):
        """Save PDB file before tailing for CpHMD

        A temporary PDB file is saved before doing modifications for CpHMD simulations.
        :param fileName: PDB file name to save for temporary structure;
                         If not specified, a file named '*_tmp.pdb' will be saved.
        :type fileName: str

        :returns: saved PDB file name
        :rtype: str
        """
        # Save PDB file without capping
        if not bool(fileName):
            fileName = self.tmpFileName
        else:
            self.tmpFileName = fileName
        PDBFile.writeFile(self.fixer.topology, self.fixer.positions, open(fileName, 'w'))
        return self.tmpFileName

    def addCapping(self, fileName=None):
        """Add capping ligands to chain ends

        For each chain, add ACE to the N terminal and add NH2 to the C terminal.
        :param fileName: The name of the final fixed PDB file that is CpHMD-ready;
                         If not specified, the name should be like '*_fixed.pdb'.
        :type fileName: str

        :returns: The name of the final fixed PDB file that is CpHMD-ready.
        :rtype: str
        """
        self.saveStandardPDB(fileName=self.tmpFileName)
        if not bool(fileName):
            fileName = self.fixedFileName
        else:
            self.fixedFileName = fileName
        # Now add capping ligands to chain ends
        with open(self.tmpFileName, 'r') as f1, open('tmp1.pdb', 'w') as f2:
            # f2.write(f1.readline())
            # f2.write(f1.readline())
            h2Lines = []  # Lines for N-terminal H2 atoms, which should be removed with H3 atoms
            oxtLines = []  # Lines for C-terminal OXT atom, which should be replaced by N atom in NH2 group
            terLines = []  # Lines for TER info
            atomLines = [[]]
            for line in f1:
                if (line[0:3] == 'TER' or line[0:4] == 'ATOM') and len(line[17:20].strip()) != 3:
                    continue  # Not a AA residue, but a RNA or DNA residue, which is not supported yet.
                if line[0:3] == 'TER':
                    terLines.append(line)
                    atomLines.append([])
                elif line[0:4] == 'ATOM':
                    if '  H2 ' in line:
                        h2Lines.append(line)
                    elif 'OXT' in line:
                        oxtLines.append(line)
                    elif '  H3 ' not in line:
                        atomLines[-1].append(line)
                elif line[0:6] == 'REMARK' or line[0:6] == 'CRYST1':
                    f2.write(line)  # Two import comment lines
            if not len(terLines) == len(h2Lines) == len(oxtLines):
                raise Exception('Chain info in the PDB file generated by addMissing.py are not consistent.')
            for i in range(len(terLines)):
                # Change 'H2' atom in hnLines[0] to 'C' atom in 'ACE' group; we only use H2 atom
                nTerm = list(h2Lines[i])
                nTerm[0:26] = 'ATOM      0  C   ACE {}   0'.format(nTerm[21])
                nTerm[-4] = 'C'
                f2.write(''.join(nTerm))
                # Copy normal atoms
                for line in atomLines[i]:
                    f2.write(line)
                # Change 'OXT' atom in oxtLine to 'N' atom in 'NHE' group
                cTerm = list(oxtLines[i])
                maxResid = int(oxtLines[i][22:26].strip())
                cTerm[11:26] = '  N   NHE {}{:>4d}'.format(cTerm[21], maxResid+1)
                cTerm[-4] = 'N'
                f2.write(''.join(cTerm))
                f2.write(terLines[i])
            f2.write('END')

        fixer = PDBFixer(filename='tmp1.pdb')
        fixer.findMissingResidues()
        fixer.findMissingAtoms()
        fixer.addMissingAtoms()
        fixer.addMissingHydrogens(7.0)  # Perhaps we can leave this out if we don't want to modify PDBfixer.

        PDBFile.writeFile(fixer.topology, fixer.positions, open('tmp2.pdb', 'w'))

        # Adjust to Amber format
        with open('tmp2.pdb', 'r') as f1, open(fileName, 'w') as f2:
            # We deal with disulfide bonds first.
            cysIds = []
            for i, line in enumerate(f1):
                if 'HG  CYS' in line:
                    cysIds.append(line[22:26])
            # Now, change residue names to the right ones.
            f1.seek(0)
            for i, line in enumerate(f1):
                if 'HETATM' in line:
                    tmp = list(line)
                    tmp[0:6] = 'ATOM  '  # Because pdbfixer use HETATM instead of ATOM for NME/NH2/ACE groups
                    line = ''.join(tmp)
                if 'ASP' in line and ('ASP' in self.residueTypes or 'AS2' in self.residueTypes):
                    tmp = list(line)
                    tmp[17:20] = 'AS2'
                    line = ''.join(tmp)
                if 'GLU' in line and ('GLU' in self.residueTypes or 'GL2' in self.residueTypes):
                    tmp = list(line)
                    tmp[17:20] = 'GL2'
                    line = ''.join(tmp)
                if 'HIS' in line and ('HIS' in self.residueTypes or 'HIP' in self.residueTypes):
                    tmp = list(line)
                    tmp[17:20] = 'HIP'
                    line = ''.join(tmp)
                # pdbfixer rename chain ids to 'A B C...'
                # the below solution means up to 7 chains can be supported.
                chain = {0: 'A', 1: 'B', 2: 'C', 3: 'D', 4: 'E', 5: 'F', 6: 'G', 7: 'H'}
                newChainID = [chain[j] for j in range(len(self.chainID))]
                for chain in newChainID:
                    if 'NH2 {} '.format(chain) in line:
                        tmp = list(line)
                        tmp[17:23] = 'NHE {} '.format(chain)  # Because 'NH2 ARG' is in the line of ARG N atom.
                        line = ''.join(tmp)
                if 'CYS' in line and line[22:26] not in cysIds:
                    tmp = list(line)
                    tmp[17:20] = 'CYX'
                    line = ''.join(tmp)
                f2.write(line)
        self.fixedFileName

    def removeAllHydrogens(self, fileName=''):
        """Remove all Hydrogen atoms

        Some Hydrogen atoms may not have standard atom names that are recognized by Amber/tleap.
        Apply this to remove all Hydrogen atoms and rely on Amber/tleap to build them back.
        :param fileName: PDB file that needs to remove Hydrogen atoms.
        :type fileName: str

        :returns: None
        :rtype: None
        """
        nonHydrogenLines = []
        if not bool(fileName):
            fileName = self.pdbFile
        with open(fileName, 'r') as ff:
            for line in ff:
                if len(line) > 12 and line[0] == 'A':
                    if not (line[12] == 'H' or line[13] == 'H'):
                        nonHydrogenLines.append(line)
                else:
                    nonHydrogenLines.append(line)
        with open(fileName, 'w') as ff:
            for line in nonHydrogenLines:
                ff.write(line)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="""Full processes to prepare an initial PDB structure
                                                that doesn't need to select models and can be read by
                                                tleap with CpHMD parameter sets. The final PDB file
                                                is named '*fixed.pdb'.""")
    parser.add_argument('-f', '--pdbfile', default=None, dest='pdbFile', metavar='',
                        help='An input standard PDB file [default: None]')
    parser.add_argument('-p', '--pdbid', default=None, dest='pdbID', metavar='',
                        help='PDB id to retrieve from RCSB [default: None]')
    parser.add_argument('-c', '--chainid', default='A', dest='chainID', metavar='',
                        help="Chain id: the single letter chain label in a PDB file  [default: 'A']")
    parser.add_argument('-mu', '--mutate', default=False, dest='mutate', action='store_true',
                        help="Mutate pattern: perform mutation on certain residues [default: False]")
    parser.add_argument('-rm', '--removeHydrogens', default=True, dest='removeHydrogens', action='store_true',
                        help="If true, Hydrogen atoms are removed before doing any fix [default: True]")
    parser.add_argument('-type', '--residueTypes', default="GLU ASP HIS CYS LYS", dest='residueTypes', metavar='',
                        help="Titratable residue types [default: 'GLU ASP HIS CYS LYS']")
    args = parser.parse_args()
    if not (args.pdbID or args.pdbFile):
        parser.error('No PDB ID or PDB file name are provided.')
    phmdPDBPreparer(**vars(args))
