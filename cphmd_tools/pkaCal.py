#!/usr/bin/env python
# coding: utf-8

"""
    This program extract infomation from GPU-CpHMD lambda files
     and calculate pKa's of all titratable residues.
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import glob
import re
import urllib.request
import os.path
from Bio.PDB.MMCIF2Dict import MMCIF2Dict
from matplotlib import rc
from math import log10

PKA = 'p' + 'K' + "\u2090"  # pka string to print out


class pKa_cal():
    def __init__(
        self, Temp=300, delta_t=0.002, pH_file_names={},
        file_wild_card="*_prod1.lambda", delimeter='_',
        titr_types=['Asp', 'Glu', 'His', 'Cys', 'Lys'],
        identifier='', cphmd_pdb='',
    ):
        """
        params
        ------
        Temp: float, CpHMD simulation temperature for the lambda particles,
                 default is 300(K).
        delta_t: float, CpHMD simulation time-step, default is 0.002(ps).
        file_wild_card: str, wild card of all CpHMD output lambda files,
                 default is `*_prod1.lambda`.
        delimeter: char, delimeter to separate infomation in the file name.
            For example, `*_pH7.0_*` has  `_` as delimeter. Default is `_`.
        pH_file_names: dict, CpHMD output lambda file names corresponding
            to the pH conditions. If files given by `file_wild_card`
            don't all have pH info as `pH???` (surrouned by delimeters), you
            have to use `pH_file_names` to provide infomation to the class.
        titr_types: list of str, titratable residue types. Default containes
            all five types.
        identifier: str, an identifier for output files or PDB ID for matching
            the real residue IDs to CpHMD residues IDs. Default is `''` in which
            case the first four letters in the `file_wild_card` will be used.
        cphmd_pdb: str, the file name of the PDB file used in CpHMD simulations.
            Default is `''`.
        supported_types: list of str, the supported residue types in CpHMD simulations.
            Default is `['Asp', 'Glu', 'His', 'Cys', 'Lys']`.

        return
        ------
        The class itself
        """
        self.Temp = Temp
        self.delta_t = delta_t
        self.pH_file_names = pH_file_names
        self.file_wild_card = file_wild_card
        self.delimeter = delimeter
        self.titr_types = titr_types
        self.identifier = identifier
        self.cphmd_pdb = cphmd_pdb
        # pH_file_names is generated or updated below
        self._get_pH_file_names_from_file_wild_card()
        self.supported_types = [
            'Asp',
            'Glu',
            'His',
            'Cys',
            'Lys',
        ]  # These residue types are titratable in current Amber-CpHMD
        for titr_type in self.titr_types:
            if titr_type not in self.supported_types:
                print(
                    """
                    Residue type '{}' is not supported.
                    Only '{}' are supported.
                    """
                    .format(titr_type, self.supported_types)
                )
                raise Exception(ValueError)
        self.default_pKas = {
            'Asp': [3.7],
            'Glu': [4.2],
            'His': [6.6, 6.1],
            'Cys': [8.5],
            'Lys': [10.4],
        }  # Used as initial guess of their pKa's
        his_1 = self.default_pKas['His'][0]
        his_2 = self.default_pKas['His'][1]
        self.default_pKas['His'] = [-log10(10**(-his_1)+10**(-his_2))]
        # Begin and end time steps for calculating
        self.begin_step = 0
        self.end_step = -1
        # Lower and higher bounds for lambda and x as physical states
        self.lower_bound = 0.2
        self.higher_bound = 0.8
        self.dpi = 150  # DPI for saved figures
        # Get data from lambda files and calculate S values
        self._get_all_data()
        if not (self.cphmd_pdb and os.path.isfile(self.cphmd_pdb)):
            self._find_lowest_pH_park()
        self._get_S_values()
        # Results
        self.results_of_restypes = {}
        self.restype_resids = {}
        # Plot settings
        self.font = {
            'family': 'serif',
            'serif': 'Helvetica',
            'sans-serif': 'Helvetica',
            'weight': 'normal',
            'size': 14,
        }
        self.line = {
            'linestyle': '-',
            'linewidth': 4,
        }
        self.scatter = {'edgecolors': 'k', 'marker': '^'}
        self.apply_style()
        self.LINE_COLOR = '#F535AA'  # neon pink
        self.SCATTER_COLOR = 'k'

    def _find_lowest_pH_park(self):
        """
        Determine the park values for each residue types at the lowest pH
        """
        self.lowest_park = {}
        num_0 = 0  # number of cys/lys
        num_4 = 0  # number of asp/glu
        cyslys_park_1, cyslys_park_2, his_park = '', '', ''
        aspglu_park_1, aspglu_park_2 = '', ''
        for res, tauto in zip(self.ires, self.itauto):
            if tauto == '0':
                num_0 += 1
                cyslys_park_tmp = self.park[self.p_min][res]
                if num_0 == 1:
                    cyslys_park_1 = cyslys_park_tmp
                elif num_0 >= 2 and cyslys_park_tmp != cyslys_park_1:
                    cyslys_park_2 = cyslys_park_tmp
            elif tauto == '2':
                his_park = self.park[self.p_min][res]
            elif tauto == '4':
                num_4 += 1
                aspglu_park_tmp = self.park[self.p_min][res]
                if num_4 == 1:
                    aspglu_park_1 = aspglu_park_tmp
                elif num_4 >= 2 and aspglu_park_tmp != aspglu_park_1:
                    aspglu_park_2 = aspglu_park_tmp
            if (cyslys_park_1 and cyslys_park_2 and aspglu_park_1
                    and aspglu_park_2 and his_park):
                break
        # Assign park values to Cys or Lys for type '0'
        if cyslys_park_1 == '':  # Non Cys/Lys found
            if 'Lys' in self.titr_types or 'Cys' in self.titr_types:
                print(
                    """
                    Warning: neither 'Lys' nor 'Cys' is found titratable
                    from the lambda files. You wouldn't be able to find
                    data for them.
                    """
                )
            self.lowest_park['Lys'] = cyslys_park_1
            self.lowest_park['Cys'] = cyslys_park_2
        elif cyslys_park_2 == '':  # only one of Lys or Cys
            if 'Lys' in self.titr_types and 'Cys' in self.titr_types:
                print(
                    """
                    Only 'Lys' or 'Cys' is found titratable
                    from the lambda files. Please specify the
                    correct one.
                    """
                )
                raise Exception(ValueError)
            elif 'Lys' in self.titr_types and 'Cys' not in self.titr_types:
                self.lowest_park['Lys'] = cyslys_park_1
                self.lowest_park['Cys'] = cyslys_park_2
            elif 'Lys' in self.titr_types and 'Cys' not in self.titr_types:
                self.lowest_park['Cys'] = cyslys_park_1
                self.lowest_park['Lys'] = cyslys_park_2
            else:
                print(
                    """
                    Either 'Lys' or 'Cys' is found titratable
                    from the lambda files. Please specify the
                    correct one.
                    """
                )
                raise Exception(ValueError)
        else:  # both Lys and Cys
            if not ('Lys' in self.titr_types and 'Cys' in self.titr_types):
                print(
                    """
                    Warning: both 'Lys' and 'Cys' are found titratable
                    from the lambda files even if at least one of them is
                    not in the 'titr_types' list.
                    """
                )
            if float(cyslys_park_1) > float(cyslys_park_2):
                self.lowest_park['Lys'] = cyslys_park_1
                self.lowest_park['Cys'] = cyslys_park_2
            else:
                self.lowest_park['Lys'] = cyslys_park_2
                self.lowest_park['Cys'] = cyslys_park_1

        # Assign park values to Asp or Glu for type '4'
        if aspglu_park_1 == '':  # Non Asp/Glu found
            if 'Asp' in self.titr_types or 'Glu' in self.titr_types:
                print(
                    """
                    Warning: neither 'Asp' nor 'Glu' is found titratable
                    from the lambda files. You wouldn't be able to find
                    data for them.
                    """
                )
            self.lowest_park['Asp'] = aspglu_park_1
            self.lowest_park['Glu'] = aspglu_park_2
        elif aspglu_park_2 == '':  # only one of Asp or Glu
            if 'Asp' in self.titr_types and 'Glu' in self.titr_types:
                print(
                    """
                    Only 'Asp' or 'Glu' is found titratable
                    from the lambda files. Please specify the
                    correct one.
                    """
                )
                raise Exception(ValueError)
            elif 'Asp' in self.titr_types and 'GLu' not in self.titr_types:
                self.lowest_park['Asp'] = aspglu_park_1
                self.lowest_park['Glu'] = aspglu_park_2
            elif 'GLu' in self.titr_types and 'Asp' not in self.titr_types:
                self.lowest_park['Glu'] = aspglu_park_1
                self.lowest_park['Asp'] = aspglu_park_2
            else:
                print(
                    """
                    Either 'Asp' or 'Glu' is found titratable
                    from the lambda files. Please specify the
                    correct one.
                    """
                )
                raise Exception(ValueError)
        else:  # both Asp and Glu
            if not ('Asp' in self.titr_types and 'Glu' in self.titr_types):
                print(
                    """
                    Warning: both 'Asp' and 'Glu' are found titratable
                    from the lambda files even if at least one of them is
                    not in the 'titr_types' list.
                    """
                )
            if float(aspglu_park_1) > float(aspglu_park_2):
                self.lowest_park['Glu'] = aspglu_park_1
                self.lowest_park['Asp'] = aspglu_park_2
            else:
                self.lowest_park['Glu'] = aspglu_park_2
                self.lowest_park['Asp'] = aspglu_park_1

        # Assign His type
        self.lowest_park['His'] = his_park
        if his_park and 'His' not in self.titr_types:
            print(
                """
                Warning: 'His' is found titratable from the lambda files
                even if it is not in the 'titr_types' list.
                """
            )
        elif his_park == '' and 'His' in self.titr_types:
            print(
                """
                Warning:  'His' is not titratable
                from the lambda files. You wouldn't be able to find
                data for it.
                """
            )

    def _get_pH_file_names_from_file_wild_card(self):
        """
        Get pH_file_names from files given by file_wild_card.
        """
        if self.file_wild_card:
            for f in sorted(glob.glob(self.file_wild_card),
                            key=self._numericalSort):
                name = f.split('_')[0]
                if not self.identifier and len(name) == 4:
                    self.identifier = f[0:4]
                pH_token = [i for i in f.split(self.delimeter) if 'pH' in i]
                pH = float(pH_token[0][2:])
                self.pH_file_names.update({pH: f})
        self.p_min = min(self.pH_file_names.keys())
        self.p_max = max(self.pH_file_names.keys())

    def _get_all_data(self):
        """
        Extracts all data from the lambda files.
        Expects pH info surrounded by delimeters in the file names.

        param
        -----
        delimeter: char, character to separate pH information.

        returns
        -------
        Values for (self.pHs, self.ititr, self.ires, self.itauto,
                    self.park, self.time_steps, self.lambda_values,
                    self.x_values, self.file_names).
            pHs: list of float, pH conditions for all lambda files.
            file_names: dict of str, file names for all pHs
            ititr: list of str, ith titratable residues.
            ires: list of str, all residue ids.
            itauto: list of str, residue types defined by CpHMD inputparm file.
            park: dict of dict of float, park nums of all residues for all pHs.
            time_steps: dict of list of float, time steps for all pHs.
            lambda_values: dict of dict of list of float, lambda values of
                           all residues for all pHs.
            x_values: dict of dict of list of float, x values of
                           all residues for all pHs.
        """
        self.time_steps = {}
        self.lambda_values = {}
        self.x_values = {}
        self.park = {}
        i = 0
        for pH in self.pH_file_names:
            self.time_steps[pH] = []
            self.lambda_values[pH] = {}
            self.x_values[pH] = {}
            with open(self.pH_file_names[pH], 'r') as lf:
                if i == 0:
                    ititr_line = lf.readline()
                    self.ititr = ititr_line.split()[2:]
                    ires_line = lf.readline()
                    self.ires = ires_line.split()[2:]
                    self.unique_ires = set(ires_line.split()[2:])
                    itauto_line = lf.readline()
                    self.itauto = itauto_line.split()[2:]
                for res in self.unique_ires:
                    self.lambda_values[pH][res] = []
                    self.x_values[pH][res] = []
                for line in lf:
                    if line[0] == '#' and line[0:6] != '# ParK':
                        continue
                    elif line[0:6] == '# ParK':
                        park_of_a_pH = dict(zip(self.ires, line.split()[2:]))
                        self.park[pH] = dict(park_of_a_pH)
                        continue
                    values = [float(j) for j in line.split()]
                    # Now update time_steps, lambda_values, and x_values
                    self._update_step_lambda_x(pH, values)
            i += 1

    def _get_resids_per_type(self, restype):
        """
        An internal function to get all residue ids of a residue type

        param
        -----
        restype: str, residue type name

        return
        -----
        resids: list of str, all residue ids of type by 'restype'
        """
        resids = []
        if restype not in self.supported_types:
            print(
                """
                Only types in '{}' are supported.
                """
                .format(self.supported_types)
            )
            return []
        if not (self.cphmd_pdb and os.path.isfile(self.cphmd_pdb)):
            for res in self.unique_ires:
                if self.park[self.p_min][res] == self.lowest_park[restype]:
                    resids.append(res)
        else:
            if restype == 'Glu':
                match = 'GL2'
            elif restype == 'Asp':
                match = 'AS2'
            elif restype == 'His':
                match = 'HIP'
            elif restype == 'Lys':
                match = 'LYS'
            elif restype == 'Cys':
                match = 'CYS'
            else:
                match = restype
            all_resids = []
            with open(self.cphmd_pdb, 'r') as cf:
                for line in cf:
                    if len(line) > 27:
                        tmp_restype = line[17:20]
                        tmp_resid = line[20:26].strip()
                        if tmp_restype == match:
                            all_resids.append(tmp_resid)
            resids = list(set(all_resids))
        resids.sort(key=int)
        return resids

    def _update_step_lambda_x(self, pH, values):
        """
        An internal function to update time step, lambda, and x values after
        reading a new time-step data.

        params
        ------
        pH: the pH condition for the file containing the current line
        values: the list of float numbers consisting of each time-step data.
                values[0] is time step and values[1:] are lambda and x values.

        return
        ------
        Update the tuple of (self.time_step, self.lambda_values, self.x_values)

        """
        self.time_steps[pH].append(values[0])
        for res, tauto, value in zip(self.ires, self.itauto, values[1:]):
            if tauto == '0':  # 'Cys' and 'Lys' types
                self.lambda_values[pH][res].append(value)
                self.x_values[pH][res].append(0.0)
                # 0.0 for Cys and Lys x_values
            elif tauto == '1' or tauto == '3':
                # lambda values for 'Glu', 'Asp', and 'His' types
                self.lambda_values[pH][res].append(value)
            else:
                # x values for 'Glu', 'Asp', and 'His' types
                self.x_values[pH][res].append(value)

    def _numericalSort(self, value):
        """
        An internal function used in the glob function key to sort file names
        according to numerical values.
        """
        numbers = re.compile(r'(\d+)')
        parts = numbers.split(value)
        parts[1::2] = map(int, parts[1::2])
        return parts

    def _hh_equation(self, pH, hill, pKa):
        """
        An internal function defines the Henderson-Hasselbasch equation

        params
        ------
        pH: float, pH value
        hill: float, Hill coefficient
        pKa: float, pKa value

        return
        ------
        1 / (1 + 10**(hill*(pKa - pH))): float, unprotonation fraction (S)
        """
        return 1 / (1 + 10**(hill*(pKa - pH)))

    def get_tautomer(self, resid):
        """
        Calculate tautomer states.

        param
        -----
        resid: str, residue id

        return
        -----
        xs: dict of float, ratios of x=1 state for all pHs
        """
        xs = {}
        for pH in self.x_values:
            zero_xs = 0
            one_xs = 0
            for i, x_in_a_step in enumerate(self.x_values[pH][resid]):
                lbda = self.lambda_values[pH][resid][i]
                if lbda < self.lower_bound or lbda > self.higher_bound:
                    if x_in_a_step < self.lower_bound:
                        zero_xs += 1
                    elif x_in_a_step > self.higher_bound:
                        one_xs += 1
            if zero_xs + one_xs == 0:
                print(
                    """
                    Warning: all states are mixed states
                    for residue {} at pH={:.1f}.
                    """
                    .format(resid, pH)
                )
                xs[pH] = None
            else:
                xs[pH] = one_xs / (zero_xs + one_xs)
        return xs

    def plot_time_series(
        self, resid, lambda_or_x='lambda', plot_file='', show_plot=True,
        single_fig_size=(5.5, 5.5), save_plot=True, plot_together=True,
        stack_fig_width=14, col=4, transparent=True, scatter_color='',
    ):
        """
        Plot lambda time series of a residue for all pHs.

        params
        ------
        resid: str, residue id in ires
        lambda_or_x: str, plot 'lambda' or 'x' time series
        save_plot: bool, whether to save all plots
        plot_together: whether to plot together time series for all pHs
        col: int, number of columns to plot together.
        show_plot: bool, whether to show all plots
        """
        resid_show = resid
        # TO DO: match to real resids in original PDB file
        # It should have the format like 'A_His70'
        if not scatter_color:
            scatter_color = self.SCATTER_COLOR
        if lambda_or_x == 'lambda':
            data = self.lambda_values
        elif lambda_or_x == 'x':
            data = self.x_values
        else:
            print("Choose 'lambda' or 'x' for 'lambda_or_x'.")
            raise Exception("InputError: wrong input for 'lambda_or_x'.")
        if plot_together:
            N = len(data)
            if N % col == 0:
                row = N // col
            else:
                row = N // col + 1
            stack_fig_length = row * (stack_fig_width / col)
            fig, axs = plt.subplots(
                row, col,
                figsize=(stack_fig_width, stack_fig_length),
                sharey=True, sharex=False,
            )
        yticks = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
        ytick_labels = ['{:.1f}'.format(i) for i in yticks]
        n_pH = 0  # nth pH condition
        for pH in data:
            data_of_a_pH = data[pH][resid]
            time_of_a_pH = [step*self.delta_t*0.001
                            for step in self.time_steps[pH]]
            if not plot_together:
                plt.figure(figsize=single_fig_size)
                plt.scatter(time_of_a_pH, data_of_a_pH, color=scatter_color)
                plt.yticks(yticks, ytick_labels)
                plt.title(
                    '{} vs. Time for Residue {} at pH={:.1f}'
                    .format(lambda_or_x, resid_show, pH),
                )
                plt.xlabel('Time (ns)')
                plt.ylabel(lambda_or_x)
                plot_file = '{}_time_Resid{}_pH{}.png'.format(
                    lambda_or_x, resid_show, pH,
                )
                plt.tight_layout(pad=0.4, rect=[0, 0.03, 1, 0.95])
                if save_plot:
                    plt.savefig(plot_file, dpi=self.dpi, transparent=True)
            else:
                i, j = n_pH // col, n_pH % col
                axs[i, j].scatter(
                    time_of_a_pH, data_of_a_pH, color=scatter_color,
                )
                axs[i, j].set_title('pH = {}'.format(pH))
                if j == 0:
                    axs[i, j].set_ylabel(lambda_or_x)
                if (i == row - 1
                        or (N % col != 0 and i == row - 2 and j >= N % col)):
                    axs[i, j].set_xlabel('Time (ns)')
                else:
                    axs[i, j].set_xticklabels([])
                if not plot_file:
                    plot_file = '{}_time_Resid{}.png'.format(
                        lambda_or_x, resid_show,
                    )
                if N % col != 0:
                    for j in range(N % col, col):
                        axs[row - 1, j].axis('off')
                fig.suptitle(
                    '{} vs. Time for Residue {}'
                    .format(lambda_or_x, resid_show),
                )
                fig.tight_layout(
                    pad=0.4,
                    w_pad=0.5,
                    h_pad=0.0,
                    rect=[0, 0.03, 1, 0.95],
                )
            n_pH += 1
        if save_plot:
            fig.savefig(plot_file, dpi=self.dpi, transparent=transparent)
        if show_plot:
            plt.show()
        plt.close()

    def _get_S_values(self, **kwargs):
        """
        Calculate unprotonation fraction (S) values of all residues for all pHs

        return
        -----
        unprot_fracs: dict of float, unprot frac values for all pHs
        mix_fracs: dict of float, mixed states for all pHs
        """
        self.unprot_fracs = {}
        self.mix_fracs = {}
        for res in self.ires:
            self.unprot_fracs[res] = dict({})
            self.mix_fracs[res] = dict({})
            for pH in self.x_values:
                zero_s = 0
                one_s = 0
                mix = 0
                for i, l_in_a_step in enumerate(self.lambda_values[pH][res][self.begin_step:self.end_step]):
                    x = self.x_values[pH][res][i]
                    if x < self.lower_bound or x > self.higher_bound:
                        if l_in_a_step < self.lower_bound:
                            zero_s += 1
                        elif l_in_a_step > self.higher_bound:
                            one_s += 1
                        else:
                            mix += 1
                    else:
                        mix += 1
                if zero_s + one_s == 0:
                    print(
                        """
                        Warning: all states are mixed states
                        for residue {} at pH={:.1f}.
                        """
                        .format(res, pH)
                    )
                    self.unprot_fracs[res][pH] = None
                    self.mix_fracs[res][pH] = 1.0
                else:
                    self.unprot_fracs[res][pH] = one_s / (zero_s + one_s)
                    self.mix_fracs[res][pH] = mix / (mix + zero_s + one_s)

    def get_titration_by_types(
        self, restypes, identifier='',
        pka_file='pKa.csv', simple_output=True, plot_type='by_restype',
        single_fig_size=(5.5, 5.5), stack_fig_width=14, show_plot=True,
        save_plot=True, transparent=True, line_color='', scatter_color='',
    ):
        """
        Get titration curves and fit pKa & Hill values
            for a list of residue types

        params
        -----
        restypes: list of str, residue types to get titration analysis
        identifier: str, a file identifier to save titration data
        pka_file: str, file name to save pKa and Hill results
        simplt_output: bool, whether Hill values should be ommited
        plot_type: str, plot titration curves 'by_restype', 'by_resid',
                    'both', or 'None'
        show_plot: bool, whether to show the plots

        return
        ------
        results_of_restypes: titration results of all residue types
                             in the restypes list.
        """
        if not identifier:
            identifier == self.identifier
        if not line_color:
            line_color = self.LINE_COLOR
        if not scatter_color:
            scatter_color = self.SCATTER_COLOR
        with open(pka_file, 'w') as pf:
            if simple_output:
                pf.write('Resname,Resid,Chain,pKa')
            else:
                pf.write('Residue,pKa,pKa_err,Hill,Hill_err')
            for restype in restypes:
                self.results_of_restypes[restype] = dict({})
                resids = self._get_resids_per_type(restype)
                if not resids:
                    continue
                for resid in resids:
                    data_of_resid = self.unprot_fracs[resid]
                    pHs = [key for key in data_of_resid]
                    S = [value for value in data_of_resid.values()]
                    if self.restype_resids:
                        resid_show = self.restype_resids[restype][resid]
                    else:
                        resid_show = resid  # real resid in RCSB PDB file
                    file_name = '{}-{}.data'.format(
                        self.identifier, resid_show,
                    )
                    with open(file_name, 'w') as sf:
                        sf.write('pH,S')
                        for pH, s in zip(pHs, S):
                            sf.write('\n{:.1f},{:.3f}'.format(pH, s))
                    pKa0 = self.default_pKas[restype][0]
                    popt, pcov = self.fit_hh_equation(pHs, S, (1, pKa0))
                    self.results_of_restypes[restype][resid] = {
                        'residue_type': restype,
                        'real_resid': resid_show,
                        'pKa': popt[1], 'pKa_err': pcov[1][1],
                        'Hill': popt[0], 'Hill_err': pcov[0][0],
                    }
                    if simple_output:
                        r_n, c_s = resid_show.split('_')
                        if type(popt[1]) is str:
                            pf.write(
                                '\n{},{},{},{}'
                                .format(restype, r_n, c_s, popt[1])
                            )
                        else:
                            pf.write(
                                '\n{},{},{},{:.1f}'
                                .format(restype, r_n, c_s, popt[1])
                            )
                    else:
                        if type(popt[1]) is float:
                            pf.write(
                                '\n{}{},{:.2f},{:.3f},{:.2f},{:.2f}'
                                .format(
                                    restype, resid_show, popt[1],
                                    pcov[1][1], popt[0], pcov[0][0],
                                )
                            )
                        else:
                            pf.write(
                                '\n{}{},{},{:.3f},{},{:.2f}'
                                .format(
                                    restype, resid_show, popt[1],
                                    pcov[1][1], popt[0], pcov[0][0],
                                )
                            )
                    if plot_type == 'by_resid' or plot_type == 'both':
                        fit_results = self.results_of_restypes[restype][resid]
                        self._plot_titration_by_resid(
                            resid, data_of_resid, fit_results,
                            fig_size=single_fig_size, show_plot=show_plot,
                            save_plot=save_plot, transparent=transparent,
                            line_color=line_color, scatter_color=scatter_color,
                        )
                # plots and fitting results.
                if plot_type == 'by_restype' or plot_type == 'both':
                    self.plot_titration_by_type(
                        restype, stack_fig_width=stack_fig_width,
                        show_plot=show_plot, save_plot=save_plot,
                        transparent=transparent, line_color=line_color,
                        scatter_color=scatter_color,
                    )

    def fit_hh_equation(self, pHs, S, init_params=(1, 7.0)):
        """
        Fit Henderson-Hasselbach equation according to titration data

        params
        -----
        pHs: list of float, pH values
        S: list of float, unprotonation fraction values
        initial_params: tuple of float, initial guess of Hill and pKa values

        return
        ------
        A tuple of (fit_params, pcov)
        fit_params: fitted results for [pKa, Hill_coeff]
        pcov: same as pcov in curve_fit of scipy
        """
        s_mean = np.mean(S)
        p_max = max(pHs)
        p_min = min(pHs)
        p_mean = np.mean(pHs)
        fit_params = [0, 0]
        pcov = [[0, 0], [0, 0]]
        try:
            popt, pcov = curve_fit(
                self._hh_equation, pHs, S, p0=init_params,
            )
            if popt[0] <= 0:   # Hill coefficient cannot be negative
                if s_mean <= 0.5:
                    fit_params[1], fit_params[0] = '> {}'.format(p_max), 'NaN'
                elif s_mean > 0.5:
                    fit_params[1], fit_params[0] = '< {}'.format(p_min), 'NaN'
                else:
                    fit_params[1], fit_params[0] = '~ {}'.format(p_mean), 'NaN'
            elif p_min - popt[1] > 1.0:
                fit_params[1], fit_params[0] = '<< {}'.format(p_min), 'NaN'
            elif popt[1] - p_max > 1.0:
                fit_params[1], fit_params[0] = '>> {}'.format(p_max), 'NaN'
            else:
                fit_params[1], fit_params[0] = popt[1], popt[0]
        except RuntimeError:  # Cannot fit with initial params
            if s_mean < 0.5:  # Almost all protonated
                init_params = [1, p_max]
                try:
                    popt, pcov = curve_fit(
                        self._hh_equation, pHs, S, p0=init_params,
                    )
                    if popt[0] <= 0:   # Hill coefficient cannot be negative
                        fit_params[1] = '> {}'.format(p_max)
                        fit_params[0] = 'NaN'
                    elif popt[1] - p_max > 1.0:
                        fit_params[1] = '>> {}'.format(p_max)
                        fit_params[0] = 'NaN'
                    else:
                        fit_params[1], fit_params[0] = popt[1], popt[0]
                except RuntimeError:
                    fit_params[1], fit_params[0] = '>> {}'.format(p_max), 'NaN'
            elif s_mean > 0.5:  # Almost all deprotonated
                init_params = [1, p_min]
                try:
                    popt, pcov = curve_fit(
                        self._hh_equation, pHs, S, p0=init_params,
                    )
                    if popt[0] <= 0:  # Hill coefficient cannot be negative
                        fit_params[1] = '< {}'.format(p_min)
                        fit_params[0] = 'NaN'
                    elif p_min - popt[1] > 1.0:
                        fit_params[1] = '<< {}'.format(p_min)
                        fit_params[0] = 'NaN'
                    else:
                        fit_params[1], fit_params[0] = popt[1], popt[0]
                except RuntimeError:
                    fit_params[1] = '<< {}'.format(min(pHs))
                    fit_params[0] = 'NaN'
            else:
                fit_params[1], fit_params[0] = '~{}'.format(p_mean), 'NaN'
        return fit_params, pcov

    def _set_ticks(self, tick_values=[]):
        """
        A function to set the tick values and labels

        params
        ------
        tick_values: list of floats, values for ticks;
                    default is [] and the whole pH values are used.
        """
        if not tick_values:
            if len(self.pH_file_names) < 6:
                tick_values = [pH for pH in self.pH_file_names]
            else:
                pHs = [pH for pH in self.pH_file_names]
                tick_min = int(min(pHs)*2) * 0.5
                tick_max = int(max(pHs)*2) * 0.5 + 0.5
                intvl_values = [0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5]
                num_intvls = [int((tick_max-tick_min)*10)//int(i*10)
                              for i in intvl_values]
                num_intvls_3 = [abs(int((tick_max-tick_min)*10)//int(i*10)-3)
                                for i in intvl_values]
                # number of intervals closes to 3
                min_num = min(num_intvls_3)
                min_index = num_intvls_3.index(min_num)
                n, v = num_intvls[min_index], intvl_values[min_index]
                tick_values = [tick_min+v*i for i in range(n+1)]
        tick_labels = ['{:.1f}'.format(pH) for pH in tick_values]
        return tick_values, tick_labels

    def _plot_titration_by_resid(
        self, resid, pH_S_of_resid, fit_results,
        fig_size=(5.5, 5.5), show_plot=True, save_plot=True, plot_file='',
        transparent=True, line_color='', scatter_color='',
    ):
        """
        Plot the titration curve of a resid

        params
        -----
        resid: str, residue id in ires.
        pH_S_of_resid: dict, pH vs S data of the resid.
        fit_results: dict, fitted results for the residue titration data.
        plot_file: str, file name of the saved plot.
        fig_size: tuple of float, figure size.
        show_plot: bool, whether to show the plot.
        """
        if not line_color:
            line_color = self.LINE_COLOR
        if not scatter_color:
            scatter_color = self.SCATTER_COLOR
        plt.figure(figsize=fig_size)
        pHs = [key for key in pH_S_of_resid]
        S = [value for value in pH_S_of_resid.values()]
        resid_show = fit_results['real_resid']
        restype = fit_results['residue_type']
        if not plot_file:
            plot_file = '{}_{}{}_titration.png'.format(
                self.identifier, restype, resid_show,
            )
        pKa = fit_results['pKa']
        # pKa_error = fit_results['pKa_err']
        Hill_coeff = fit_results['Hill']
        # Hill_coeff_error = fit_results['Hill_err']
        if Hill_coeff == 'NaN':
            plt.scatter(
                pHs, S,
                label='{}{}, {} {}'.format(
                    restype, resid_show, PKA, pKa,
                ), color=scatter_color,
            )
        else:
            plt.scatter(pHs, S, color=scatter_color)
            pHs_aug = np.linspace(min(pHs), max(pHs), num=100)
            plt.plot(
                pHs_aug, self._hh_equation(pHs_aug, Hill_coeff, pKa),
                label='{}{}, {} = {:.2f}'.format(
                    restype, resid_show, PKA, pKa,
                ), color=line_color,
            )
        x_ticks, x_tick_labels = self._set_ticks()
        y_ticks = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
        y_tick_labels = ['{:.1f}'.format(i) for i in y_ticks]
        plt.xticks(x_ticks, x_tick_labels)
        plt.yticks(y_ticks, y_tick_labels)
        plt.legend()
        plt.xlabel('pH')
        plt.ylabel('Unprot Frac')
        plt.tight_layout()
        if save_plot:
            plt.savefig(plot_file, dpi=self.dpi, transparent=transparent)
        if show_plot:
            plt.show()
        plt.close()

    def plot_titration_by_type(
        self, restype, plot_file='', single_fig_size=(5.5, 5.5),
        stack_fig_width=14, plot_together=True, col=4, show_plot=False,
        save_plot=True, transparent=True, line_color='', scatter_color='',
    ):
        """
        Plot all titration curves of a residue type

        params:
        -----
        restype: str, residue type to plot
        plot_file: str, file name to save the plot
        fig_width: float, width of the figure
        save_plot: bool, whether to save the figure
        plot_together: bool, whether to plot titration curves into one figure
        col: int, number of columns in the plot if plot_together
        show_plot: bool, whether to show the plots
        """
        if not line_color:
            line_color = self.LINE_COLOR
        if not scatter_color:
            scatter_color = self.SCATTER_COLOR
        resids = self._get_resids_per_type(restype)
        if not resids:
            return
        if plot_together:
            N = len(resids)
            if N < col:
                col = N
                row = 1
            elif N % col == 0:
                row = N // col
            else:
                row = N // col + 1
            if col > N:
                col = N
            stack_fig_length = row * (stack_fig_width / col)
            fig, axs = plt.subplots(
                row, col,
                figsize=(stack_fig_width, stack_fig_length),
                sharey=True, sharex=False,
            )
        xticks, xtick_labels = self._set_ticks()
        yticks = [0.0, 0.2, 0.4, 0.6, 0.8, 1.0]
        ytick_labels = ['{:.1f}'.format(i) for i in yticks]
        if not plot_file:
            plot_file = '{}_{}_titration.png'.format(self.identifier, restype)
        for n_res, resid in enumerate(resids):
            data_of_resid = self.unprot_fracs[resid]
            fit_results = self.results_of_restypes[restype][resid]
            if not plot_together:
                self._plot_titration_by_resid(
                    resid, data_of_resid, fit_results,
                    fig_size=single_fig_size, show_plot=show_plot,
                    save_plot=save_plot, transparent=transparent,
                    line_color=line_color, scatter_color=scatter_color,
                )
            else:
                i, j = n_res // col, n_res % col
                pHs = [key for key in data_of_resid]
                S = [value for value in data_of_resid.values()]
                resid_show = fit_results['real_resid']
                pKa = fit_results['pKa']
                Hill_coeff = fit_results['Hill']
                if row > 1:
                    if Hill_coeff == 'NaN':
                        axs[i, j].scatter(
                            pHs, S, label='{} {}'.format(PKA, pKa),
                            color=scatter_color,
                        )
                    else:
                        axs[i, j].scatter(pHs, S, color=scatter_color)
                        pHs_aug = np.linspace(min(pHs), max(pHs), num=100)
                        hh_S = self._hh_equation(pHs_aug, Hill_coeff, pKa)
                        axs[i, j].plot(
                            pHs_aug, hh_S,
                            label='{} = {:.2f}'.format(PKA, pKa), color=line_color,
                        )
                    axs[i, j].set_ylim([0.0, 1.0])
                    axs[i, j].set_yticks(yticks)
                    axs[i, j].set_yticklabels(ytick_labels)
                    axs[i, j].set_xticklabels([])
                    axs[i, j].legend()
                    axs[i, j].set_title('{}'.format(resid_show))
                    if j == 0:
                        # Set y-axis labels
                        axs[i, j].set_ylabel('Unprot Frac')
                    if (i == row - 1
                            or (N % col != 0 and i == row - 2 and j >= N % col)):
                        # Set x-axis labels
                        axs[i, j].set_xlabel('pH')
                        axs[i, j].set_xticks(xticks)
                        axs[i, j].set_xticklabels(xtick_labels)
                elif col > 1:
                    if Hill_coeff == 'NaN':
                        axs[j].scatter(
                            pHs, S, label='{} {}'.format(PKA, pKa),
                            color=scatter_color,
                        )
                    else:
                        axs[j].scatter(pHs, S, color=scatter_color)
                        pHs_aug = np.linspace(min(pHs), max(pHs), num=100)
                        hh_S = self._hh_equation(pHs_aug, Hill_coeff, pKa)
                        axs[j].plot(
                            pHs_aug, hh_S,
                            label='{} = {:.2f}'.format(PKA, pKa), color=line_color,
                        )
                    axs[j].set_ylim([0.0, 1.0])
                    axs[j].set_yticks(yticks)
                    axs[j].set_yticklabels(ytick_labels)
                    axs[j].set_xticklabels([])
                    axs[j].legend()
                    axs[j].set_title('{}'.format(resid_show))
                    if j == 0:
                        # Set y-axis labels
                        axs[j].set_ylabel('Unprot Frac')
                    # Set x-axis labels
                    axs[j].set_xlabel('pH')
                    axs[j].set_xticks(xticks)
                    axs[j].set_xticklabels(xtick_labels)
                else:
                    if Hill_coeff == 'NaN':
                        axs.scatter(
                            pHs, S, label='{} {}'.format(PKA, pKa),
                            color=scatter_color,
                        )
                    else:
                        axs.scatter(pHs, S, color=scatter_color)
                        pHs_aug = np.linspace(min(pHs), max(pHs), num=100)
                        hh_S = self._hh_equation(pHs_aug, Hill_coeff, pKa)
                        axs.plot(
                            pHs_aug, hh_S,
                            label='{} = {:.2f}'.format(PKA, pKa), color=line_color,
                        )
                    axs.set_ylim([0.0, 1.0])
                    axs.set_yticks(yticks)
                    axs.set_yticklabels(ytick_labels)
                    axs.set_xticklabels([])
                    axs.legend()
                    axs.set_title('{}'.format(resid_show))
                    if j == 0:
                        # Set y-axis labels
                        axs.set_ylabel('Unprot Frac')
                    # Set x-axis labels
                    axs.set_xlabel('pH')
                    axs.set_xticks(xticks)
                    axs.set_xticklabels(xtick_labels)
        if row > 1 and N % col != 0:
            for j in range(N % col, col):
                axs[row - 1, j].axis('off')
        fig.suptitle('{} {} Titration'.format(self.identifier, restype))
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        if save_plot:
            fig.savefig(plot_file, dpi=self.dpi, transparent=transparent)
        if show_plot:
            plt.show()
        plt.close()

    def bar_plot(
        self, restypes, same_protein=True, save_plot=True, show_plot=True,
        colors={'Single': {
            'edge': 'k', 'prot': 'r', 'mix': 'white', 'unprot': 'b'},
        },
        fig_size=(5.5, 4.0), alpha=0.7, width=0.8, transparent=True,
        cap_size=7,
    ):
        """
        A function to make bar plots of pKa's for residue types.

        -------
        Params:
        restypes: list of str, residue types to plot
        same_protein: bool, whether all chains are the same protein; if 'True',
            pKa's of same resids of different chains will be plotted together.
            If they are not the same protein, 'True' can give out unexpected
            results; if they are the same protein, 'False' may actually be what
            you want to present; if the resids are not matched to 'resid_chain'
            format, the pKa's of the same resids of different chains won't be
            plotted together anyway.
        colors: dict, 'prot' for resids whose pKa's are less than lower pH
            range, 'grey' for resids whose pKa's have determined value, and
            'unprot' for resides whose pKa's are larger than upper pH range.

        -------
        Returns:
        None
        """
        for restype in restypes:
            try:
                self.results_of_restypes[restype]
                if not self.results_of_restypes[restype]:
                    continue
            except KeyError:
                self.get_titration_by_types(
                    [restype], save_plot=False, plot_type='by_restype',
                    show_plot=False)
            resids_fit_results = self.results_of_restypes[restype]
            default_chain = 'Single'
            tmp_chain = ''
            pkas_to_show = {}
            errs_to_show = {}
            resids_to_show = {}
            high_pkas_to_show = {}  # for pKa's >> p_max
            high_errs_to_show = {}  # for pKa's >> p_max
            low_pkas_to_show = {}  # for pKa's << p_min
            low_errs_to_show = {}  # for pKa's << p_min
            first = 0   # Determine whether it's the first in loop
            num_chains = 0  # Number of chains
            pka_max = 0
            for resid in resids_fit_results:
                first += 1
                resid_to_show = resids_fit_results[resid]['real_resid']
                resid_chain = resid_to_show.split('_')
                if len(resid_chain) == 2:
                    if resid_chain[1] != tmp_chain:
                        num_chains += 1
                        tmp_chain = resid_chain[1]
                        try:
                            colors[tmp_chain]
                        except KeyError:
                            colors[tmp_chain] = colors['Single']
                        pkas_to_show[tmp_chain] = []
                        errs_to_show[tmp_chain] = []
                        resids_to_show[tmp_chain] = []
                        high_pkas_to_show[tmp_chain] = []
                        high_errs_to_show[tmp_chain] = []
                        low_pkas_to_show[tmp_chain] = []
                        low_errs_to_show[tmp_chain] = []
                elif first == 1:
                    tmp_chain = default_chain
                    pkas_to_show[tmp_chain] = []
                    errs_to_show[tmp_chain] = []
                    resids_to_show[tmp_chain] = []
                    high_pkas_to_show[tmp_chain] = []
                    high_errs_to_show[tmp_chain] = []
                    low_pkas_to_show[tmp_chain] = []
                    low_errs_to_show[tmp_chain] = []
                resids_to_show[tmp_chain].append(resid_chain[0])
                pka = resids_fit_results[resid]['pKa']
                if type(pka) is not np.float64:
                    if pka[0:2] == '>>' or pka[0:2] == '> ':
                        high_pkas_to_show[tmp_chain].append(self.p_max + 5.0)
                        high_errs_to_show[tmp_chain].append(0)
                        pkas_to_show[tmp_chain].append(0)
                        errs_to_show[tmp_chain].append(0)
                        low_pkas_to_show[tmp_chain].append(0)
                        low_errs_to_show[tmp_chain].append(0)
                    elif pka[0:2] == '<<' or pka[0:2] == '< ':
                        low_pkas_to_show[tmp_chain].append(self.p_min + 0.05)
                        low_errs_to_show[tmp_chain].append(0)
                        pkas_to_show[tmp_chain].append(0)
                        errs_to_show[tmp_chain].append(0)
                        high_pkas_to_show[tmp_chain].append(0)
                        high_errs_to_show[tmp_chain].append(0)
                elif pka > self.p_max:
                    if pka > pka_max:
                        pka_max = pka
                    high_pkas_to_show[tmp_chain].append(self.p_max + 5.0)
                    high_errs_to_show[tmp_chain].append(0)
                    pkas_to_show[tmp_chain].append(0)
                    errs_to_show[tmp_chain].append(0)
                    low_pkas_to_show[tmp_chain].append(0)
                    low_errs_to_show[tmp_chain].append(0)
                elif pka < self.p_min:
                    low_pkas_to_show[tmp_chain].append(self.p_min + 0.05)
                    low_errs_to_show[tmp_chain].append(0)
                    high_pkas_to_show[tmp_chain].append(0)
                    high_errs_to_show[tmp_chain].append(0)
                    pkas_to_show[tmp_chain].append(0)
                    errs_to_show[tmp_chain].append(0)
                else:
                    if pka > pka_max:
                        pka_max = pka
                    pkas_to_show[tmp_chain].append(pka)
                    errs_to_show[tmp_chain].append(
                        resids_fit_results[resid]['pKa_err'])
                    low_pkas_to_show[tmp_chain].append(0)
                    low_errs_to_show[tmp_chain].append(0)
                    high_pkas_to_show[tmp_chain].append(0)
                    high_errs_to_show[tmp_chain].append(0)
            if same_protein:
                fig = plt.figure(figsize=fig_size)
                file_name = '{}_pKa_barplot.png'.format(restype)
                x = np.arange(len(pkas_to_show[tmp_chain])) * num_chains
            else:
                x = np.arange(len(pkas_to_show[tmp_chain]))
            n_chains = 0
            for chain in pkas_to_show:
                title = '{} {} {}'.format(
                        self.identifier, restype, PKA)
                if not same_protein:
                    fig = plt.figure(figsize=fig_size)
                    file_name = '{}_{}_pKa_barplot.png'.format(restype, chain)
                    title = '{} chain {} {} {}'.format(
                        self.identifier, chain, restype, PKA)
                    colors[chain]['edge'] = 'k'
                    colors[chain]['prot'] = 'r'
                    colors[chain]['unprot'] = 'b'
                    label = None
                else:
                    label = 'chain {}'.format(chain)
                plt.bar(
                    x+n_chains*width, pkas_to_show[chain],
                    yerr=errs_to_show[chain], alpha=alpha,
                    fc=colors[chain]['mix'], width=width, lw=1.5,
                    ec=colors[chain]['edge'],
                    label=label, capsize=cap_size,
                )
                high_label = None
                if any(high_pkas_to_show[chain]):
                    high_label = '{} > {}'.format(PKA, self.p_max)
                plt.bar(
                    x+n_chains*width, high_pkas_to_show[chain], lw=1.5,
                    alpha=alpha, fc=colors[chain]['unprot'], width=width,
                    capsize=cap_size, ec=colors[chain]['edge'],
                    label=high_label,
                )
                low_label = None
                if any(low_pkas_to_show[chain]):
                    low_label = '{} < {}'.format(PKA, self.p_min)
                plt.bar(
                    x+n_chains*width, low_pkas_to_show[chain], lw=1.5,
                    alpha=alpha, fc=colors[chain]['prot'], width=width,
                    capsize=cap_size, ec=colors[chain]['edge'],
                    label=low_label
                )
                if same_protein:
                    n_chains += 1
                plt.ylim([self.p_min, min(self.p_max, pka_max+0.5)])
                angle = 0
                if len(x) > 15:
                    angle = 45
                plt.xticks(
                    ticks=x, labels=resids_to_show[chain], rotation=angle)
                plt.xlabel('Residue number')
                plt.ylabel(PKA)
                plt.title(title)
                if not same_protein:
                    plt.axhline(
                        y=self.default_pKas[restype][0], ls='--', color='k',
                        lw=1.5, label='model {}={:.1f}'.format(
                            PKA, self.default_pKas[restype][0]))
                    plt.legend()
                    plt.tight_layout()
                    if show_plot:
                        plt.show()
                    if save_plot:
                        fig.savefig(
                            file_name, dpi=self.dpi, transparent=transparent)
                    plt.close()
            if same_protein:
                plt.axhline(
                    y=self.default_pKas[restype][0], ls='--', color='k',
                    lw=1.5, label='model {}={:.1f}'.format(
                        PKA, self.default_pKas[restype][0]))
                plt.legend()
                plt.tight_layout()
                if show_plot:
                    plt.show()
                if save_plot:
                    fig.savefig(
                        file_name, dpi=self.dpi, transparent=transparent)
                plt.close()

    def match_resids(self, mmcif_file='', chain_ids=['A'], pdb_id=''):
        """
        A function to find out the resids in the original CIF file
        given the resids of a type in the CpHMD lambda files.
        """
        # # Initiate the self.restype_resids dictionary as follows
        # self.restype_resids =
        # {restype1: {resid1: r_resid1, resid2: r_resid2...}, restype2: {}...}
        # r_resid1 is 'resid_chainid' if matched, 'resid' if not matched.
        if self.restype_resids:
            return  # If it's provided then exit
        restype_N_resids = {}  # {restype1: N1, restype2: N2...}
        cphmd_restype_resids = {}  # {restype1: [resid1, resid2...], ...}
        for restype in self.titr_types:
            cphmd_restype_resids[restype] = self._get_resids_per_type(restype)
            # If fails to match, just use the resids used in cphmd pdb file.
            self.restype_resids[restype] = dict(zip(
                cphmd_restype_resids[restype],
                cphmd_restype_resids[restype]))
            restype_N_resids[restype] = len(cphmd_restype_resids[restype])
        # # Make sure a mmcif_file is in the directory
        if not (mmcif_file or pdb_id or self.identifier):
            print('No PDB ID or mmCIF file is provided. The CpHMD resids are used in results reports.')
            return
        elif mmcif_file:
            pdb_id = False
            try:
                f = open(mmcif_file, 'r')
                f.close()
            except:
                print('Cannot open the mmcif file {}.'.format(mmcif_file))
                raise  # This is the only case for raising an exception
        elif not pdb_id:
            pdb_id = self.identifier
        if pdb_id:
            url = 'https://files.rcsb.org/download/{}.cif'.format(pdb_id)
            mmcif_file = '{}.cif'.format(pdb_id)
            try:
                urllib.request.urlretrieve(url, mmcif_file)
            except:
                print(
                    """
                    Failed to fetch '{}' from RCSB. Check your internect
                    connection and try again. Now the CpHMD resids are used in results reports.
                    """
                    .format(mmcif_file))
                return
        mmcif_dict = MMCIF2Dict(mmcif_file)
        real_seq_chains = mmcif_dict['_pdbx_poly_seq_scheme.pdb_strand_id']
        overlap_chain_ids = []  # Make sure chain ids are overlapped
        for chain in chain_ids:
            if chain not in real_seq_chains:
                print(
                    """
                    Warning: chain {} in the 'chain_ids'
                    not found in the mmcif file.
                    """
                    .format(chain)
                )
            else:
                overlap_chain_ids.append(chain)
        chain_ids = overlap_chain_ids
        real_seq_resids = mmcif_dict['_pdbx_poly_seq_scheme.pdb_seq_num']
        real_seq_resnames = mmcif_dict['_pdbx_poly_seq_scheme.mon_id']
        real_resid_chain = [
            '{}_{}'.format(i, j)
            for i, j in zip(real_seq_resids, real_seq_chains)]
        resid_chainid_resname = dict(zip(real_resid_chain, real_seq_resnames))
        # The above dictionary format is thus {'1 A': 'Asp'}
        # # Now sort the dict by restype
        real_restype_resids = {}  # {restype: [r_resid1, r_resid2, ...], ...}
        real_restype_N_resids = {}  # {restype: N_r_resids, ...}
        # number of resids of all types in crystal structure
        matched_types = []
        # The types which have same number of resids as in the mmcif file sequence
        restype_offsets = {}  # {restype: [(resid1, r_resid1, offset1), ...], ...}
        # resid offsets for each MATCHED type. For one type, if their is no
        # dislocation, all offsets should be the same.
        for restype in self.titr_types:
            real_restype_resids[restype] = [
                resid_chainid
                for resid_chainid, resname
                in resid_chainid_resname.items()
                if resname == restype.upper()
                and resid_chainid.split('_')[1]
                in chain_ids
            ]
            real_restype_N_resids[restype] = len(real_restype_resids[restype])
            if real_restype_N_resids[restype] == restype_N_resids[restype] != 0:
                matched_types.append(restype)
                offsets = [
                    (i, j, int(i) - int(j.split('_')[0]))
                    for i, j
                    in zip(
                        cphmd_restype_resids[restype],
                        real_restype_resids[restype]
                    )
                ]
                restype_offsets[restype] = offsets
                self.restype_resids[restype] = dict(
                    zip(
                        cphmd_restype_resids[restype],
                        real_restype_resids[restype])
                )  # matched_types are returned here!
        if matched_types == self.titr_types:
            # All titr_types are matched!
            return
        elif matched_types:
            # There is at least one matched type and we shall use them as anchors
            restype_disloc_positions = {}
            # {restype1: [((disloc_resid1, r_disloc_resid1, offset1),
            # (disloc_resid2, r_disloc_resid2, offset2)), ...], ...}
            # individual disloc positions for each matched restype
            restype_N_dislocs = {}  # {restype1: N_disloc1, ...}
            # number of dislocs for each matched restype
            no_disloc_found = True  # no matched restypes in dislocation
            for restype in matched_types:
                restype_disloc_positions[restype] = []
                if len(restype_offsets[restype]) > 1:
                    for i, j in zip(restype_offsets[restype][0:-1],
                                    restype_offsets[restype][1:]):
                        if i[2] - j[2] != 0:  # a disloc happends!
                            no_disloc_found = False
                            # We save the position after disloc
                            restype_disloc_positions[restype].append((i, j))
                restype_N_dislocs[restype] = len(restype_disloc_positions[restype])
            if no_disloc_found:
                # just need to apply the universal offset to unmatched restypes
                for restype in self.titr_types:
                    if restype not in matched_types:
                        # mached_types are dealt above
                        # for unmatched ones, we need to delete some from ends in real sequence
                        shifted_resids = []
                        for resid_chainid in real_restype_resids[restype]:
                            r_resid, chainid = resid_chainid.split('_')
                            resid = int(r_resid) + restype_offsets[matched_types[0]][0][2]
                            if str(resid) in cphmd_restype_resids[restype]:
                                shifted_resids.append(resid_chainid)
                        try:
                            self.restype_resids[restype] = dict(
                                zip(
                                    cphmd_restype_resids[restype],
                                    shifted_resids)
                            )
                        except:
                            print(
                                """
                                Warning: residue type {} has resids not found in crystal structure.
                                resids used by cphmd is returned.
                                """
                                .format(restype))
                            continue
            else:
                # Identify the restype with most dislocs
                type_with_max_num_dislocs = max(
                    restype_N_dislocs, key=restype_N_dislocs.get)
                max_disloc_num = restype_N_dislocs[type_with_max_num_dislocs]
                # determine overall disloc positions first round
                max_disloc_offsets = {}
                # {restype1: [((resid1, r_resid1, offset1), (resid2, r_resid2, offset2)), ...], ...}
                # all disloc positions
                max_disloc_types = []  # residue types with max dislocs
                for restype in matched_types:
                    if restype_N_dislocs[restype] == max_disloc_num:
                        max_disloc_types.append(restype)
                        max_disloc_offsets[restype] = restype_disloc_positions[restype]
                cphmd_resid_lows = {}  # lower resids for each disloc
                real_resid_lows = {}  # lower real resids for each disloc
                offset_lows = {}  # lower resid offset for each disloc
                cphmd_resid_highs = {}  # higher resids for each disloc
                real_resid_highs = {}  # higher real resids for each disloc
                offset_highs = {}  # higher resid offset for each disloc
                divide_resid_low = {}  # highest of lower resids for each disloc
                divide_resid_high = {}  # lowest of higher resids for each disloc
                resid_lows = {}  # dict of lower_resid: lower_real_resid for each disloc
                resid_highs = {}  # dict of higer_resid: higer_real_resid for each disloc
                resid_offset_lows = {}  # dict of lower_resid: lower_resid_offset for each disloc
                resid_offset_highs = {}  # dict of higher_resid: lower_resid_offset for each disloc
                for i in range(max_disloc_num):
                    # ith dislocation
                    cphmd_resid_lows[i] = [
                        max_disloc_offsets[rtype][i][0][0] for rtype in max_disloc_types]
                    real_resid_lows[i] = [
                        max_disloc_offsets[rtype][i][0][1] for rtype in max_disloc_types]
                    offset_lows[i] = [
                        max_disloc_offsets[rtype][i][0][2] for rtype in max_disloc_types]
                    cphmd_resid_highs[i] = [
                        max_disloc_offsets[rtype][i][1][0] for rtype in max_disloc_types]
                    real_resid_highs[i] = [
                        max_disloc_offsets[rtype][i][1][1] for rtype in max_disloc_types]
                    offset_highs[i] = [
                        max_disloc_offsets[rtype][i][1][2] for rtype in max_disloc_types]
                    divide_resid_low[i] = max(cphmd_resid_lows[i], key=int)
                    divide_resid_high[i] = min(cphmd_resid_highs[i], key=int)
                    resid_lows[i] = dict(zip(cphmd_resid_lows[i], real_resid_lows[i]))
                    resid_offset_lows[i] = dict(zip(cphmd_resid_lows[i], offset_lows[i]))
                    resid_highs[i] = dict(zip(cphmd_resid_highs[i], real_resid_highs[i]))
                    resid_offset_highs[i] = dict(zip(cphmd_resid_highs[i], offset_highs[i]))
                for restype in self.titr_types:
                    if restype not in matched_types:
                        shifted_resids = []
                        # all_prev_divide_resid_high = 0  # previous high
                        for resid in cphmd_restype_resids[restype]:
                            prev_divide_resid_high = 0  # previous high
                            for i in range(max_disloc_num):
                                if prev_divide_resid_high <= int(resid) <= int(divide_resid_low[i]):
                                    real_divide_resid_low = resid_lows[divide_resid_low[i]]  # to get chain info only
                                    chain_id = real_divide_resid_low.split('_')[1]
                                    r_resid = int(resid) - resid_offset_lows[i][resid]
                                    shifted_resids.append(str(r_resid)+'_'+chain_id)
                                    break
                                elif int(divide_resid_low[i]) < int(resid) < int(divide_resid_high[i]):
                                    print(
                                        """
                                        Warning: residue {} in CpHMD PDB file cannot be matched.
                                        It will be returned as 'real' residue ID.
                                        """
                                        .format(resid)
                                    )
                                    shifted_resids.append(resid)
                                    break
                                elif i == max_disloc_num - 1:
                                    real_divide_resid_high = resid_highs[divide_resid_high[i]]
                                    chain_id = real_divide_resid_high.split('_')[1]
                                    r_resid = int(resid) - resid_offset_highs[i][resid]
                                    shifted_resids.append(str(r_resid)+'_'+chain_id)
                                else:
                                    prev_divide_resid_high = divide_resid_high[i]
                        try:
                            self.restype_resids[restype] = dict(
                                zip(
                                    cphmd_restype_resids[restype],
                                    shifted_resids)
                            )
                        except:
                            print(
                                """
                                Warning: residue type {} has resids not found in crystal structure.
                                resids used by cphmd is returned.
                                """
                                .format(restype))
                            continue
        else:
            print(
                """
                Warning: cannot match resids. resids used by cphmd is returned.
                Please specify them as a dictionary key: value where key is restype and
                value is a list of resids in crystal structure.
                """
            )
            return

    def apply_style(self):
        """
        Apply fonts, lines, and scatter styles after changing rc settings.
        """
        rc('font', **self.font)  # pass in the font dict as kwargs
        rc('lines', **self.line)
        rc('scatter', **self.scatter)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="""A Python program to calculate all the pKa values
        of selected residue types.""")
    parser.add_argument(
        '-f', '--files', default='*prod1.lambda', dest='files',
        metavar='FILES', help="""Wildcard for lambda files generated
        by CpHMD simulations. [default: *prod1.lambda]""")
    parser.add_argument(
        '-id', '--identifier', default=None, dest='id', metavar='ID',
        help="""An identifier (e.g. PDB ID) to label analysis result files.
        [default: None]""")
    parser.add_argument(
        '-mf', '--mmcif', default='', dest='mmcif', metavar='MMCIF',
        help="""An MMCIF file which contains detailed information of the protein.
        [default: None]""")
    parser.add_argument(
        '-cp', '--cphmdpdb', default='', dest='cphmdpdb',
        metavar='CPHMDPDB', help="""The fixed PDB file for
        CpHMD simulations [default: '']""")
    parser.add_argument(
        '-b', '--beginstep', default=0, dest='beginstep', metavar='BEGINSTEP',
        help="""The beginning frame to calculate unprotonation fraction.
        [default: 0]""")
    parser.add_argument(
        '-e', '--endstep', default=-1, dest='endstep', metavar='ENDSTEP',
        help="""The end frame to calculate unprotonation fraction.
        [default: -1]""")
    parser.add_argument(
        '-lb', '--lowbound', default=0.2, dest='lowbound',
        metavar='LOWBOUND', help="""The fraction value below which is
        considered as being protonated. [default: 0.2]""")
    parser.add_argument(
        '-hb', '--highbound', default=0.8, dest='highbound',
        metavar='HIGHBOUND', help="""The fraction value above which is
        considered as being unprotonated. [default: 0.8]""")
    parser.add_argument(
        '-c', '--chains', default='A', dest='chains', metavar='CHAINS',
        help="""The chains in a PDB file; if multiple, the need to be separated
        by common delimeters and quoted. [default: None]""")
    parser.add_argument(
        '-t', '--resduetypes', default='Lys Cys', dest='restypes',
        metavar='RESTYPE', help="""The residue types need to calculate their
        pKa values. [default: 'Lys Cys']""")
    args = parser.parse_args()
    cal = pKa_cal(
        identifier=args.id, file_wild_card=args.files, cphmd_pdb=args.cphmdpdb)
    cal.match_resids(chain_ids=args.chains.split(), mmcif_file=args.mmcif, pdb_id=args.id)
    cal.lower_bound = args.lowbound
    cal.higher_bound = args.highbound
    cal.begin_step = args.beginstep
    cal.end_step = args.endstep
    cal.dpi = 600
    cal.get_titration_by_types(
        restypes=args.restypes.split(), show_plot=False,
        scatter_color='k', line_color='m', plot_type='by_restype',
        transparent=False,
    )
    cal.dpi = 600
    cal.bar_plot(
        restypes=args.restypes.split(), cap_size=10, width=0.6,
        show_plot=False, fig_size=(8, 6), alpha=0.9, same_protein=False,
    )
