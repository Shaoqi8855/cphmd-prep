#! /bin/bash

##################################################################
# Prepare Input Files for CpHMD simulations                      #
##################################################################


####################################
# Take care of inputs              #
####################################

# Default job settings
me=`basename "$0"`                      # This script
POSITIONAL=()                           # For parsing command line arguments
PDBID=''                                # PDB ID for which a PDB file can be downloaded from RCSB
PDBFILE=''                              # PDB file name 
CHAINID='A'                             # Default chain selection
MODELID=''                              # No alternative model in default PDB files
istest=false                            # If true, tepmorary files are not deleted
outfiletype='zip'                       # Output files will be in a 'zip' file or a 'tar.gz' file depending on input. 
pHmin=6.5                               # Default minimum pH value
pHmax=9.0                               # Default maximum pH value
pHintvl=0.5                             # Default pH interval
ionic=0.15                              # Default ionic strength 0.15M
simlength=10                            # Default simulation length 10ns
temperature=300                         # Default simulation temperature 300K
residuetypes='Asp Glu His Cys Lys'      # Default titratable residues
mode='async'                            # CpHMD run mode: ['async', 'reex', 'ind']

# Parse input arguments
usage()
{
echo "    Usage: $me [[-pdb|--pdbid]|[-fil|--pdbfile]] [-cha|--chainid]" \
    "[-mod|--modelid]" \
    "[-con|--conc]"\
    "[-tim|--time] [-tem|--temp] [-dph]" \
    "[-phl|--phlow] [-phu|--phup]" \
    "[-res|--restype] [-run|--runmode] [-h|--help]" \

cat <<EOF

    -h|--help                   This help message.
    -pdb|--pdbid                PDB ID to retrieve the corresponding PDB file from RCSB; default is ''.
    -fil|--pdbfile              PDB file name; default is ''.
                                Either a PDB ID or a PDB file name is required.
    -cha|--chainid              Chain IDs to include in simulations; default is 'A'.
    -mod|--modelid              The model ID for crystal structures refined from modeling; default is ''.
    -con|--conc                 Ionic strength in M used in the GBNeck2 implict solvent model; default is 0.15M.
    -tim|--time                 Simulation length in nano second for each pH condition; default is 10.
    -tem|--temp                 Simulation temperature in Kelvin; default is 300.
    -dph                        pH interval between lowest and highest pH conditions; default is 0.5.
    -phl|--phlow                Minimum pH condition for CpHMD simulaitons; default is 6.5.
    -phu|--phup                 Maximum pH condition for CpHMD simulations; default is 9.0.
    -res|--restype              Amino acid residue types set to be titratable in CpHMD simulations; default is 'Asp Glu His Cys Lys'.
    -run|--runmode              CpHMD run mode. 'async': use asynchronous pH replica exchange on GPU(s); 'reex': use classical pH replica exchange on CPUs; 'ind': use independent pH simulations on GPU(s)
EOF
}
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -pdb|--pdbid)
    PDBID="$2"
    shift # past argument
    shift # past value
    ;;
    -fil|--pdbfile)
    PDBFILE="$2"
    shift # past argument
    shift # past value
    ;;
    -cha|--chainid)
    CHAINID="$2"
    shift # past argument
    shift # past value
    ;;
    -mod|--modelid)
    MODELID="$2"
    shift # past argument
    shift # past value
    ;;
    -con|--conc)
    ionic="$2"
    shift # past argument
    shift # past value
    ;;
    -tim|--time)
    simlength="$2"
    shift # past argument
    shift # past value
    ;;
    -tem|--temp)
    temperature="$2"
    shift # past argument
    shift # past value
    ;;
    -dph)
    pHintvl="$2"
    shift # past argument
    shift # past value
    ;;
    -phl|--phlow)
    pHmin="$2"
    shift # past argument
    shift # past value
    ;;
    -phu|--phup)
    pHmax="$2"
    shift # past argument
    shift # past value
    ;;
    -res|--restype)
    residuetypes="$2"
    shift # past argument
    shift # past value
    ;;
    -ot|--outfiletype)
    outfiletype="$2"
    shift # past argument
    shift # past argument
    ;;
    -h|--help)
    usage
    exit  
    shift # past value
    ;;
    -t|--test)
    istest=true
    shift # past argument
    ;;    
    -b|--build)
    buildonly=true
    shift # past argument
    ;;
    -run|--runmode)
    mode="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    usage
    exit  
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

#########################
# Take care of inputs   #
#########################

# Determine this package installation path and initiate system path
cphmd="`dirname \"$0\"`"                        # this script
cphmd="`( cd \"$cphmd\" && cd .. && pwd )`"     # cphmd package directory
if [ -z "$cphmd" ]; then
    echo "Error: the $cphmd directory is not accessible."
    exit 1 
fi
export PYTHONPATH=$PYTHONPATH:$cphmd/cphmd_tools
export PATH=$PATH:$cphmd/cphmd_tools
if [[ $AMBERHOME ]]
then
    amberbin="$AMBERHOME/bin"
else
    echo "AMBERHOME is not set but required for using tleap."
    exit
fi

# Conditions used in thre prepared CpHMD calculations 
if [ "$PDBID" == '' ] && [ "$PDBFILE" == '' ]; then
	echo "Error: must provide a PDB id or a PDB file."
	usage
	exit 1
elif [ "$PDBID" != '' ] && [  "$PDBFILE" == '' ]; then
	echo "Provided PDB ID: ${PDBID}" > cphmd_prep.log
	dirname=$PDBID
    PDB_ID=$PDBID
elif [ "$PDBID" == '' ] && [ "$PDBFILE" != '' ]; then
    if [ -f "$PDBFILE" ]; then
        echo "Provided PDB file: ${PDBFILE}" > cphmd_prep.log
    else
        echo "PDB file $PDBFILE not found in the working directory." 
        exit 1
    fi
	PDB_FILE=`basename "$PDBFILE"`
    PDB_ID=$(echo $PDB_FILE | cut -c1-4)  # Use the first four letters as PDB ID in use
	dirname=$(echo ${PDB_FILE%.pdb})
else
	echo "Error: must provide either a PDB id or a PDB file, not both."
	usage
	exit 1
fi

echo "Chain ID: ${CHAINID}" >> cphmd_prep.log 

if [ "$MODELID" != '' ]; then
	echo "Model ID: ${MODELID}" >> cphmd_prep.log 
else
	MODELID='-'
fi
echo "Titratable residue types: $residuetypes" >> cphmd_prep.log

pHs=()
pH=$(echo "scale=2; $pHmin * 1 / 1" | bc)   # always float
while [ $(echo "$pH<=$pHmax" | bc) == 1 ]
do
    pHs+=($pH)
    pH=$(echo "scale=2; ($pH + $pHintvl) * 1 / 1" | bc)
done
echo "pH conditions for CpHMD: "${pHs[@]}", calculated from min pH $pHmin, max pH $pHmax, and interval $pHintvl." >> cphmd_prep.log
echo "Temperature for MD simulations: ${temperature}K" >> cphmd_prep.log 
echo "Implicit salt concentration: ${ionic}M" >> cphmd_prep.log

echo "Target simulation length for each pH: ${simlength}ns" >> cphmd_prep.log

# Check other output control parameters
if [ "$istest" = true ]; then
	echo "This is a test job. Every file will be kept." >> cphmd_prep.log
else
    echo "Only Amber pdb, parm7, rst7, mdin, and phmdin files necessary for CpHMD simulations will be generated." >> cphmd_prep.log
fi

if [ "$mode" == 'ind' ]; then
    echo "Files to run independent pH CpHMD simulations on GPU(s) will be generated." >> cphmd_prep.log
elif [ "$mode" == 'reex' ]; then
    echo "Files to run classical pH replica exchange CpHMD simulations on CPUs will be generated." >> cphmd_prep.log
elif [ "$mode" == 'async' ]; then
    echo "Files to run asynchronous pH replica exchange CpHMD simulations on GPU(s) will be generated." >> cphmd_prep.log
else
    echo "Mode $mode is not supported." >> cphmd_prep.log
    exit 1
fi

if [ "$outfiletype" != 'zip' ] && [ "$outfiletype" != 'tar.gz' ]; then
    echo "Only 'zip' and 'tar.gz' file types are supported for the final prepared file folder."
    exit 1
fi


#######################################
# prepared CpHMD ready PDB file #
#######################################
currentdir=`pwd`
py3dir=`which python3`
if [ -z $py3dir ]; then
    echo "Error: python3 is not installed."
    exit 1
else
    [[ -d $dirname ]] || mkdir $dirname
    cd $dirname
    if [ "$PDBID" != '' ]; then
        preparePDB.py -p $PDBID -c "$CHAINID" -m $MODELID -type "$residuetypes" > prepare.log
        wait
        fixedpdb=`ls ${PDBID}*_fixed.pdb`
        if [ -f "$fixedpdb" ]; then
            echo "CpHMD PDB file: ${fixedpdb}" >> ${currentdir}/cphmd_prep.log
        else
            echo "Failed to prepare a PDB file for $PDBID."
            exit 1
        fi
    else
        [[ -f $PDB_FILE ]] || cp ${currentdir}/$PDBFILE .
        preparePDB.py -f $PDB_FILE -c "$CHAINID" -m $MODELID -type "$residuetypes" > prepare.log
        wait
        fixedpdb=`ls *_fixed.pdb`
        if [ -f "$fixedpdb" ]; then
            echo "CpHMD PDB file: ${fixedpdb}" >> ${currentdir}/cphmd_prep.log
        else
            echo "Failed to prepare a PDB file for $PDBID."
            exit 1
        fi
    fi
fi
protein=$(echo ${fixedpdb%_fixed.pdb})

echo "##This BASH script should be embeded in a system specific job submission file if the job is to run in a CPU/GPU cluster.##" > README
echo "#Job information:" >> README
if [ "$PDBID" != '' ]; then
	echo "#PDB ID provided: $PDBID" >> README
else
	echo "#PDB file provided: $PDB_FILE" >> README
fi
echo "#Selected chain ID: $CHAINID." >> README
echo "#Selected model ID: $MODELID." >> README

######################
# Standard Variables #
######################
conc=$ionic   	# Salt Concentration, Implicent Salt Concentration
crysph=7.0      # Starting pH, can come from crystal structure for protein
cutoff=999.0    # Nonbond cutoff small system 9.0 ang, otherwise use 12.0 ang; for GB, use 999.0
ts=0.002	    # Time step
echo "#Implicit ionic strength: $ionic M." >> README

########################
# Minimization Options #
########################
nsteps=1000      	# Number of total minimization steps
nsdsteps=500     	# Initial amount of total minimization as SD steps
wfrq=50         	# How often to write ene. vel. temp. restart and lamb.
restraint=50.0		# restraints applied on restrainted atoms

###############################################
# Heating NOT Used for Implict Solvent Model  #
###############################################

########################
# Equilibration Options #
########################
etemp=$temperature                      # Equilibration temperature
ensteps=2000                            # Number of each equilibration step
ewfrq=100000                            # How often to write ene. vel. temp. and lamb.
ewrst=100000                            # How often to write restart 
erestraints=(5.0 2.0 1.0 0.0)           # restraints applied on restrainted atoms
ets=$ts

########################
# Production Options #
########################
lfrq=1000                                   # How often to print lambda values for heating, equil., and prod.
pnsteps=$(echo $simlength*1000*500 | bc)    # Total MD steps
pwfrq=10000                                 # How often to write trajectories; for test only
# if [ "$istest" = false ] ; then
#     pwfrq=0                                 # For pKa calculation only in CpHMD, don't output trajectories.
# fi
pwrst=10000000000                           # How often to write restart
pts=$ts
echo "#Simulation temperature: $temperature K." >> README
echo "#Simulation length: $simlength ns." >> README

# CpHMD phmdin file
IFS=' ' read -ra my_array <<< "$residuetypes"
res=""
for i in "${my_array[@]}"
do
    if [ "$i" == "Asp" ]; then
        res+="'AS2',"
    fi
    if [ "$i" == "Glu" ]; then
        res+="'GL2',"
    fi
    if [ "$i" == "His" ]; then
        res+="'HIP',"
    fi
    if [ "$i" == "Cys" ]; then
        res+="'CYS',"
    fi
    if [ "$i" == "Lys" ]; then
        res+="'LYS',"
    fi
    if [ "$i" == "Tyr" ]; then
    res+="'TYR',"
    fi
done
echo "#Residue types set as titratable: $res" >> README
echo "&phmdin
    QMass_PHMD = 10,                    		! Mass of the Lambda Particle
    Temp_PHMD = 300,                    		! Temp of the lambda particle
    phbeta = 5,                         		! Friction Coefficient (1/ps) for titration integrator
    iphfrq = 1,                         		! Frequency to update the lambda forces
    NPrint_PHMD = $lfrq,                		! How often to print the lambda
    PrLam = .true.,                     		! Should lambda be printed
    PrDeriv = .false.,                  		! Do you want to print the derivatives?
    PRNLEV = 7,                         		! Sets what is printed out, (7) normal print level
    PHTest = 0,                         		! Are lambda and theta fixed, (0) = not fixed, (1) = fixed
    MaskTitrRes(:) = $res 	                    ! Residues to include as titratable
    MaskTitrResTypes = ${#my_array[@]},                       ! number of titratable types
    QPHMDStart = .true.,                		! Initialize velocities of titration varibles with a Boltzmann distribution
    /" > ${protein}.phmdin 
echo "CpHMD phmdin file: ${protein}.phmdin" >> ${currentdir}/cphmd_prep.log
cp $cphmd/Files/gbneck2_input.parm .
inputparm='gbneck2_input.parm'
echo "CpHMD parameter file: gbneck2_input.parm" >> ${currentdir}/cphmd_prep.log

############################################
# Let tleap prepare Amber inputs           #
############################################
awk -F '' '$14 !~ /^(H)$/' ${protein}_fixed.pdb > tmp.pdb
awk -F '' '$13 !~ /^(H)$/' tmp.pdb > ${protein}_fixed.pdb
# Make build.in file
echo "# Reads in the FF
    source $cphmd/Files/leaprc.protein.ff14SB     			    # Load protein
    set default PBradii mbondi3                             	# Modifies GB radii to mbondi3 set, needed for IGB8 this is the version of the GB
    loadOff $cphmd/Files/phmd.lib                               # load modified library file, loads definitions for AS2 GL2 (ASPP AND GLUPP)
    phmdparm = loadamberparams $cphmd/Files/frcmod.phmd        	# load modifications to bond length and dihedrals
    $protein = loadPDB ${protein}_fixed.pdb                            	# Load PDB
    saveamberparm $protein $protein.parm7 $protein.rst7         # generates the parameter files and coords file
    savepdb $protein $protein.pdb_tmp                           # save the pdb file with right atom numbering scheme wrt the parm7 file
    quit
" > build.in
   
# Run tleap
$amberbin/tleap -f build.in > tleap.log
wait    
# Change the radii of SG in Cys and HD1/HE2 in HIP using processParm7.py
# Interaction between HD1(HE1) and HD2(HE2) dummy hydrogens in AS2/GL2 is also added to the exclusion list
processParm7.py -f $protein.pdb_tmp -p $protein.parm7
mv $protein.parm7 orig_$protein.parm7
mv cphmd_$protein.parm7 $protein.parm7
wait
mv $protein.pdb_tmp $protein.pdb
echo "Amber topology rst7 file: $protein.rst7" >> ${currentdir}/cphmd_prep.log
echo "Amber parameter parm7 file: $protein.parm7" >> ${currentdir}/cphmd_prep.log

echo "#Follow the following steps to run CpHMD:" >> README
################################
#  Prepare Minimization Inputs #
################################
# Generate $protein_mini.mdin
nres=$(grep "FLAG POINTERS" -A 3 ${protein}.parm7 | tail -n 1 | awk '{print $2}')
natom=$(tail $protein.pdb | grep 'HN2 NHE' | awk '{print $2}')
nrestr=$(($nres - 2)) # We don't restrain the end residues.
echo "Minimization
    &cntrl                                         ! cntrl is a name list, it stores all conrol parameters for amber
    imin = 1, maxcyc = $nsteps, ncyc = $nsdsteps, ! Do minimization, max number of steps (Run both SD and Conjugate Gradient), first 250 steps are SD
    ntx = 1,                                     ! Initial coordinates
    ntwe = 0, ntwr = $nsteps, ntpr = $wfrq,        ! Print frq for energy and temp to mden file, write frq for restart trj, print frq for energy to mdout 
    ntc = 1, ntf = 1, ntb = 0, ntp = 0,          ! Shake (1 = No Shake), Force Eval. (1 = complete interaction is calced), Use PBC (1 = const. vol.), Use Const. Press. (0 = no press. scaling)
    cut = $cutoff,                               ! Nonbond cutoff (Ang.)
    ntr = 1, restraintmask = ':3-$nrestr&!@H=',    ! restraint atoms (1 = yes), Which atoms are restrained 
    restraint_wt = $restraint,                   ! Harmonic force to be applied as the restraint
    ioutfm = 1, ntxo = 2,                        ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
    igb = 8,
    /" > ${protein}_mini.mdin  
mini_command="mpirun -n 4 \$AMBERHOME/bin/pmemd.MPI -O -i ${protein}_mini.mdin -c ${protein}.rst7 -p ${protein}.parm7 -ref ${protein}.rst7 -r ${protein}_mini.rst7 -o ${protein}_mini.out &>> mini.log"
echo "#Step 1: run Amber minimization" >> README
echo "    $mini_command" >> README
echo "Minimization input file: ${protein}_mini.mdin" >> ${currentdir}/cphmd_prep.log
    
##################################
#  Perform Equilibration  Inputs #
##################################
echo "#Step 2: run Amber equilibrations" >> README
for restn in `seq 1 ${#erestraints[@]}` # loop over number of restraints
do
    if [ $restn == 1 ]; then
        echo "Stage $restn equilibration of $protein 
            &cntrl
            imin = 0, nstlim = $ensteps, dt = $ets,
            irest = 0, ntx = 1,ig = -1,
            temp0 = $etemp,
            ntc = 2, ntf = 2, tol = 0.00001,
            ntwx = $ewfrq, ntwe = $ewfrq, ntwr = $ewrst, ntpr = $ewfrq,
            cut = $cutoff, iwrap = 0, igb = 8,           
            ntt = 3, gamma_ln = 1.0, ntb = 0,                               ! ntp (1 = isotropic position scaling)
            iphmd = 1, solvph = $crysph, saltcon = $conc,
            ntr = 1, restraintmask = ':1-${nres}&!@H=',
            restraint_wt = ${erestraints[$(($restn-1))]},
            ioutfm = 1, ntxo = 2,                                           ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
        /" > ${protein}_equil${restn}.mdin
        echo "Amber mdin file for equilibration with restraint ${erestraints[$(($restn-1))]}: ${protein}_equil${restn}.mdin" >> ${currentdir}/cphmd_prep.log
        equilstart="${protein}_mini.rst7"
        equil_command="\$AMBERHOME/bin/pmemd.cuda -O -i ${protein}_equil${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref $equilstart -r ${protein}_equil${restn}.rst7 -o ${protein}_equil${restn}.out -x ${protein}_equil${restn}.nc -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdout ${protein}_equil${restn}.lambda  -phmdrestrt ${protein}_equil${restn}.phmdrst &>> equil.log"
        echo "    $equil_command" >> README
        command="sed -i 's/QPHMDStart = .true.,/QPHMDStart = .false.,/g' ${protein}.phmdin"
        echo "    $command" >> README
    else
        echo "Stage $restn equilibration of $protein 
            &cntrl
            imin = 0, nstlim = $ensteps, dt = $ets,
            irest = 1, ntx = 5,ig = -1,
            temp0 = $etemp,
            ntc = 2, ntf = 2, tol = 0.00001,
            ntwx = $ewfrq, ntwe = $ewfrq, ntwr = $ewrst, ntpr = $ewfrq,
            cut = $cutoff, iwrap = 0, igb = 8,           
            ntt = 3, gamma_ln = 1.0, ntb = 0,                               ! ntp (1 = isotropic position scaling)
            iphmd = 1, solvph = $crysph, saltcon = $conc,
            ntr = 1, restraintmask = ':1-${nres}&!@H=',
            restraint_wt = ${erestraints[$(($restn-1))]},
            ioutfm = 1, ntxo = 2,                                           ! Fomrat of coor. and vel. trj files, write NetCDF restrt fuile for final coor., vel., and box size
        /" > ${protein}_equil${restn}.mdin
        echo "Amber mdin file for equilibration with restraint ${erestraints[$(($restn-1))]}: ${protein}_equil${restn}.mdin" >> ${currentdir}/cphmd_prep.log
        prev=$(($restn-1))
        equilstart="${protein}_equil${prev}.rst7"
        phmdstart="${protein}_equil${prev}.phmdrst"
        command="sed -i 's/PHMDRST/PHMDSTRT/g' $phmdstart"
        echo "    $command" >> README
        equil_command="\$AMBERHOME/bin/pmemd.cuda -O -i ${protein}_equil${restn}.mdin -c $equilstart -p ${protein}.parm7 -ref $equilstart -r ${protein}_equil${restn}.rst7 -o ${protein}_equil${restn}.out -x ${protein}_equil${restn}.nc -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt $phmdstart -phmdout ${protein}_equil${restn}.lambda -phmdrestrt ${protein}_equil${restn}.phmdrst &>> equil.log"
        echo "    $equil_command" >> README
    fi
done
###############################
#  Prepare Production  Inputs #
###############################
# Create input files for Production 
final_equil_stage=${#erestraints[@]}   
command="sed -i 's/PHMDRST/PHMDSTRT/g' ${protein}_equil${final_equil_stage}.phmdrst"
echo "    $command" >> README
echo "#Step 3: run CpHMD production" >> README

if [ "$mode" == 'ind' ]; then
    for pH in "${pHs[@]}"
    do
        echo "Production stage of $protein
            &cntrl
            imin = 0, nstlim = $pnsteps, dt = $pts, 
            irest = 1, ntx = 5, ig = -1, 
            tempi = $etemp, temp0 = $etemp, 
            ntc = 2, ntf = 2, tol = 0.00001,
            ntwx = $pwfrq, ntwe = 0, ntwr = $pwrst, ntpr = 0, 
            cut = $cutoff, iwrap = 0, igb = 8,
            ntt = 3, gamma_ln = 1.0, ntb = 0,
            iphmd = 1, solvph= $pH, saltcon = $conc,
            ioutfm = 1, ntxo = 2,                        
        /" > ${protein}_pH${pH}_prod.mdin
        echo "Amber mdin file for pH=${pH}: ${protein}_pH${pH}_prod.mdin" >> ${currentdir}/cphmd_prep.log
        prod_command="\$AMBERHOME/bin/pmemd.cuda -O -i ${protein}_pH${pH}_prod.mdin -c ${protein}_equil${final_equil_stage}.rst7 -p ${protein}.parm7 -r ${protein}_pH${pH}_prod1.rst7 -o ${protein}_pH${pH}_prod1.out -x ${protein}_pH${pH}_prod1.nc -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_equil${final_equil_stage}.phmdrst -phmdout ${protein}_pH${pH}_prod1.lambda -phmdrestrt ${protein}_pH${pH}_prod1.phmdrst -inf pH${pH}.mdinfo &>> prod.log &"
        echo "    $prod_command" >> README
    done
elif [ "$mode" == 'reex' ]; then
    numexchg=$(echo $simlength*500 | bc)
    rm -rf cphmd.groupfile
    for rep in "${!pHs[@]}"
        do
            echo "Production stage of $protein using Replica Exchange
                &cntrl
                imin = 0, nstlim = $lfrq, dt = $pts, numexchg=$numexchg,
                irest = 1, ntx = 5, ig = -1, 
                tempi = $etemp, temp0 = $etemp, 
                ntc = 2, ntf = 2, tol = 0.00001,
                ntwx = $pwfrq, ntwe = $pwfrq, ntwr = $pwfrq, ntpr = 500, 
                cut = $cutoff, iwrap = 0, igb = 8,
                ntt = 3, gamma_ln = 1.0, ntb = 0,
                iphmd = 1, solvph= ${pHs[$rep]}, saltcon = $conc,
                ioutfm = 1, ntxo = 2,                        
                /" > rep${rep}_prod.mdin
                /usr/bin/printf "-O -i rep${rep}_prod.mdin -c ${protein}_equil${final_equil_stage}.rst7 -p ${protein}.parm7 -phmdin ${protein}.phmdin -phmdparm $inputparm -phmdstrt ${protein}_equil${final_equil_stage}.phmdrst -phmdout rep${rep}.lambda -phmdrestrt rep${rep}.phmdrst -o rep${rep}.mdout -r rep${rep}.rst7  -x rep.nc.%03d -rem 4 -remlog rem${rep}.log -inf pH${pHs[$rep]}.mdinfo \n" $rep  >> cphmd.groupfile
        done
    echo "## Change -np value to your system specific number" >> README
    prod_command="mpirun -np 64 \$AMBERHOME/bin/pmemd.MPI -ng ${#pHs[@]} -groupfile cphmd.groupfile &>> prod.log"
    echo "    $prod_command" >> README
else    # async
    echo "##Make sure the https://gitlab.com/shenlab-amber-cphmd/async_ph_replica_exchange repository is installed and the directory is added to PYTHONPATH.##" >> README
    rexsteps=$(echo $pnsteps/1000 | bc)
    echo "Production stage of $protein
        &cntrl
        imin = 0, nstlim = 1000, dt = $pts, 
        irest = 1, ntx = 5, ig = -1, 
        tempi = $etemp, temp0 = $etemp, 
        ntc = 2, ntf = 2, tol = 0.00001,
        ntwx = $pwfrq, ntwe = 0, ntwr = 0, ntpr = 0, 
        cut = $cutoff, iwrap = 0, igb = 8,
        ntt = 3, gamma_ln = 1.0, ntb = 0,
        iphmd = 1, solvph= pH, saltcon = $conc,
        ioutfm = 1, ntxo = 2,                        
    /" > template_${protein}_prod.mdin
    cp $cphmd/cphmd_tools/apHREX.py .
    echo "Async pH replica exchange template mdin file: template_${protein}_prod.mdin" >> ${currentdir}/cphmd_prep.log
    prod_command="python apHREX.py $rexsteps template_${protein}_prod.mdin ${protein}.parm7 gbneck2_input.parm ${protein}.phmdin ${protein}_cphmd ${protein}_equil${final_equil_stage}.rst7 "${pHs[@]}""
    echo "    $prod_command" >> README
fi

# Clean up
if [ "$istest" = false ] ; then
    rm tmp.pdb tmp1.pdb tmp2.pdb *_tmp.pdb tmp*.parm7 2> /dev/null
    rm tleap.log leap.log prepare.log build.in 2> /dev/null
    rm orig*.parm7 2> /dev/null
fi

# Prepare zip/tar file
cd ..
echo "Instructions to run CpHMD simulations are in: README" >> cphmd_prep.log
if [ "$outfiletype" == 'zip' ]; then
    zip -r cphmd_inputs.zip $dirname >/dev/null 2>&1 
    echo "Inputs files are in: cphmd_inputs.zip" >> cphmd_prep.log
else
    tar -czvf cphmd_inputs.tar.gz $dirname >/dev/null 2>&1 
    echo "Inputs files are in: cphmd_inputs.tar.gz" >> cphmd_prep.log
fi

exit 
