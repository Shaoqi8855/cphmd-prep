import replica_exchange as re
import sys
import os

restart = False         # To restart from the current async pH replica exchange simulation, change this to 'True' and run the last command in README again.
phs = sys.argv[8:]
#phs = [ph for ph in phs.split()]
exchange_steps = int(sys.argv[1])
template_mdin = sys.argv[2]
parm7 = sys.argv[3]
inputparm = sys.argv[4]
phmdin = sys.argv[5]
outfoldder = sys.argv[6]
rst7 = sys.argv[7]

if not restart:
    re.do_replica_exchange(phs, exchange_steps, template_mdin, parm7, inputparm, phmdin, outfoldder+'_0', initial_restart_file=rst7, restart=False)
else:
    i = 1
    new_folder = ''
    while not new_folder:
        test_folder = outfoldder + f'_{i}'
        if os.path.isdir(test_folder):
            i += 1
            test_folder = outfoldder + f'_{i}'
        else:
            old_folder = outfoldder + f'_{i-1}'
            new_folder = test_folder
    re.do_replica_exchange(phs, exchange_steps, template_mdin, parm7, inputparm, phmdin, new_folder, initial_restart_file=rst7, restart_directory=old_folder, restart=True)
